import numpy as np
import random

class Patient:
    def __init__(self, id, surgeon, priority, master_priority, duration):
        self.id = id
        self.surgeon = surgeon
        self.priority = priority
        self.master_priority = master_priority
        self.duration = duration

class Surgeon:
    def __init__(self, id):
        self.id = id
        self.patients = []

class Block:
    def __init__(self, id, start, end):
        self.id = id
        self.start = start
        self.end = end
        self.duration = end-start
        self.cost = self.duration
        # self.id = id

class OR:
    def __init__(self, id, start, end, start_times, end_times):
        self.id = id
        self.start = start
        self.end = end
        self.blocks = []
        self.conflicts = []
        b_id = 0
        for s in start_times:
            self.conflicts.append([])
        for s in start_times:
            if s < self.start:
                continue
            for e in end_times:
                if e <= s or e > self.end:
                    continue
                b = Block(b_id, s, e)
                self.blocks.append(b)
                for i in range(len(start_times)):
                    if s<=start_times[i] and e>=end_times[i]:
                        self.conflicts[i].append(b_id)
                b_id += 1

def parse_in(filename):
    f = open(filename, 'r')
    lines = f.read().splitlines()
    f.close()
    init_nums = list(map(int, lines[0].split(' ')))
    ns = init_nums[0]
    np = init_nums[1]
    nr = init_nums[2]
    nd = init_nums[3]
    patients = []
    surgeons = []
    for i in range(ns):
        surgeons.append(Surgeon(i))
    for j in range(2, np+2):
        line = list(map(int, lines[j].split(' ')))
        id = line[0]
        s = line[1]
        p = line[2]
        if len(line)>4:
            mp = line[3]
            d = line[4]
        else:
            mp = p
            d = line[3]
        pat = Patient(id, s, p, mp, d)
        surgeons[s].patients.append(pat)
        patients.append(pat)
    start_times = list(map(int, lines[-3].split(' ')))
    end_times = list(map(int, lines[-1].split(' ')))
    ors = []
    for d in range(nd):
        ord = []
        for r in range(nr):
            line = list(map(int, lines[np+3+d*nr+r].split(' ')))
            new_or = OR(line[1], line[2], line[3], start_times, end_times)
            ord.append(new_or)
            # print('OR:', new_or.start, new_or.end)
            # for b in new_or.blocks:
                # print('     ', b.start, b.end)
        ors.append(ord)
    unavailability = []
    if len(lines)>np+nd*nr+8:
        for k in range(np+nd*nr+4, np+nd*nr+4+ns):
            line = list(map(int, lines[k].split(' ')))
            unavailability.append(line[1:])
    return ns, np, nr, nd, surgeons, patients, ors, start_times, end_times, unavailability

if __name__ == '__main__':
    ns, np, nr, nd, surgeons, patients, ors, starts, ends = parse_in('in1.txt')

    for s in surgeons:
        print('surgeon: ', s.id)
        for p in s.patients:
            print('     patient: ', p.id, p.priority, p.duration)

    for d in range(nd):
        print('DAY: ', d)
        for r in ors[d]:
            print(' OR: ', r.id, r.start, r.end)
            for b in r.blocks:
                print('     block: ', b.id, b.start, b.end)
            for c in r.conflicts:
                print('     conflicts: ', c)