import random
import math

in_dir = '../Instances-and-proximity-files_2019/INSTANCES_FINAL/1_RealLifeSurgeryTypesDatabase/'
out_dir = '../data/1_RealLifeSurgeryTypesDatabase/constant_priority/'

ranges5 = {'1,05' : (51,60),
        '1,1' : (61, 70),
        '1,15': (71,80),
        '1,2' : (81,90)}

ranges10 = {'1,05' : (141,150),
        '1,1' : (151, 160),
        '1,15': (161,170),
        '1,2' : (171,180)}

restriction = 0
ns = 2
nd = 5
nr = 1
priority_range = (1,1)

for l in ranges5.keys():
    r = ranges5[l]
    for k in range(r[0], r[1] + 1):
        inp_filename = in_dir + str(k) + '_ordays_' + str(nd*nr) + '_load_' + l + '.txt'
        out_filename = out_dir + str(k) + '_ordays_' + str(nd*nr) + '_load_' + l + 'surgeons_' + str(ns) +'.txt'

        f = open(inp_filename)
        lines = f.read().splitlines()
        f.close()

        surgeries = []
        for line in lines[10:]:
            if len(line)>1:
                surgeries.append(list(map(float, line.split('\t'))))

        np = len(surgeries)

        start_times = ['0', '8', '16', '24']
        end_times = ['8', '16', '24', '32']

        f = open(out_filename, 'w')
        f.write('{} {} {} {}\n'.format(ns, np, nr, nd))

        f.write('patients: ID surgeon priority duration\n')
        for i in range(np):
            s = random.randint(0, ns-1)
            p = random.randint(priority_range[0], priority_range[1])
            d = int(round((surgeries[i][3] + math.exp(surgeries[i][1] + (surgeries[i][2])**2/2))/15))
            f.write('{} {} {} {}\n'.format(i, s, p, d))

        f.write('ORS: day OR start end\n')
        capacity = 0
        for d in range(nd):
            for r in range(nr):
                s = start_times[random.randint(0, restriction)]
                e = 32
                f.write('{} {} {} {}\n'.format(d, r, s, e))
                capacity += int(e)-int(s)

        f.write('start times:\n')
        f.write(' '.join(start_times))
        f.write('\n')

        f.write('end times:\n')
        f.write(' '.join(end_times))
        f.write('\n')

        max_capacity = nd*nr*(int(end_times[-1])-int(start_times[0]))
        # f.write('OR capacity: '+str(capacity/max_capacity)+'\n')
        # print('OR capacity: '+str(capacity/max_capacity)+'\n')
        f.close()