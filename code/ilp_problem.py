from parse_instance import parse_in
import gurobipy as gb
import copy
import time

ncallbacks = 0
n_lc = 0

def callback_lazy(model, where):
    global ncallbacks, n_lc

    if where == gb.GRB.Callback.MIPSOL:
        ncallbacks += 1
        # generate new lazy constraint for each surgeon
        for i in range(ns):
            ss = time.time()
            # extract blocks assigned to surgeon i
            assigned_blocks = []
            for d in range(nd):
                for r in range(nr):
                    for b in range(len(ors[d][r].blocks)):
                        if round(model.cbGetSolution(y[d][r][i,b])) == 1:
                            assigned_blocks.append(ors[d][r].blocks[b].duration)
            b_count = []
            for bd in block_durations:
                b_count.append(assigned_blocks.count(bd))

            # initiate a model to calculate an optimal patient selection
            fmodel = gb.Model()
            xf = fmodel.addVars(len(surgeons[i].patients), len(assigned_blocks), vtype=gb.GRB.CONTINUOUS, ub=1, lb=0)
            # duration of assigned block cannot be exceeded
            fmodel.addConstrs(gb.quicksum(xf[j,b]*surgeons[i].patients[j].duration for j in range(len(surgeons[i].patients))) <= assigned_blocks[b] for b in range(len(assigned_blocks)))
            # each patient can be operated only once
            fmodel.addConstrs(gb.quicksum(xf[j,b] for b in range(len(assigned_blocks))) <= 1 for j in range(len(surgeons[i].patients)))
            # the objective is to operate the highest priority patients
            fmodel.setObjective(gb.quicksum(gb.quicksum(xf[j,b] for b in range(len(assigned_blocks)))*surgeons[i].patients[j].priority for j in range(len(surgeons[i].patients))), gb.GRB.MAXIMIZE)
            fmodel.optimize()
            # calculate value of assignment in the current solution
            fs = 0
            unscheduled = []
            for pat in surgeons[i].patients:
                pat_scheduled = 0
                for d in range(nd):
                    for r in range(nr):
                        for b in range(len(ors[d][r].blocks)):
                            pat_scheduled += round(model.cbGetSolution(x[d][r][pat.id, b]))
                fs += pat_scheduled*pat.priority
                if pat_scheduled == 0:
                    unscheduled.append((pat.priority, pat.duration))
            if len(unscheduled)>0:
                unscheduled.sort(key = lambda x: (x[1], -x[0]))
                shortest = unscheduled[0][1]
                print('surgeon: ', i, ' - unscheduled: ', unscheduled)
                for bd in range(len(block_durations)):
                    if block_durations[bd] >= shortest:
                        shortest = bd
                        break
            else:
                shortest = 0
            longest = min(len(unscheduled)+shortest, len(block_durations))
            print('surgeon: ', i, ', fs: ', fs, ', opt: ', round(fmodel.objVal))
            # if the current solution is not optimal, add a lazy constraint
            if round(fmodel.objVal) > fs:
                n_lc += 1;
                z_count = []
                for j in range(len(block_durations)):
                    z_temp = 0
                    for k in range(len(block_durations)*nd*block_durations[-1]):
                        z_temp += round(model.cbGetSolution(z[i,j,k]))
                    z_count.append(z_temp-1)

                fs = round(fmodel.objVal)
                model.cbLazy(gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][pat.id,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd))*pat.priority for pat in surgeons[i].patients)
                             + bigM*(len(block_durations) - gb.quicksum(z[i,bd,z_count[bd]] for bd in range(len(block_durations))))
                             >= fs )#+ gb.quicksum(z[i,bd,b_count[bd]+1]*unscheduled[bd-shortest][0] for bd in range(shortest, longest)))
                # break
            print("1 surgeon lc generation time: ", time.time() - ss)
        print("Lazy constraint generation time: ", time.time() - s)

ns, np, nr, nd, surgeons, patients, ors, start_times, end_times, unavailability = parse_in('../data/1_RealLifeSurgeryTypesDatabase/random_priority/73_ordays_5_load_1,15surgeons_5.txt')
unavailability = []

# sort blocks according to length (to manage variables 'z')
all_blocks = []
block_durations = []
for i in range(len(end_times)):
    all_blocks.append([])
    block_durations.append(end_times[i]-start_times[0])
for d in range(nd):
    for r in range(nr):
        for b in ors[d][r].blocks:
            ind = block_durations.index(b.duration)
            all_blocks[ind].append((d,r,b.id))

model = gb.Model()
model.Params.lazyConstraints = 1
y = []
x = []
bigM = 1000

for i in range(nd):
    yd = []
    xd = []
    for r in range(nr):
        yr = model.addVars(ns, len(ors[i][r].blocks), vtype=gb.GRB.CONTINUOUS, ub=1, lb=0)      # surgeon - block assignment
        xr = model.addVars(np, len(ors[i][r].blocks), vtype=gb.GRB.CONTINUOUS, ub=1, lb=0)      # patient - block assignment
        yd.append(yr)
        xd.append(xr)
    y.append(yd)
    x.append(xd)

# # manage variables 'z'
# z = model.addVars(ns, len(block_durations), len(block_durations)*block_durations[-1]*nd, vtype=gb.GRB.BINARY)
# for i in range(ns):
#     for l in range(len(block_durations)):
#         model.addConstrs(z[i,l,k] >= z[i,l,k+1] for k in range(len(block_durations)*block_durations[-1]*nd-1))
#
# for i in range(ns):
#     for bd in range(len(block_durations)):
#         model.addConstr(z[i,bd,0] == 1)
#         # model.addConstr(gb.quicksum(z[i, bd, k] for k in range(1, nd)) == gb.quicksum(y[ab_id[0]][ab_id[1]][i, ab_id[2]] for ab_id in all_blocks[bd]))
#         model.addConstr(gb.quicksum(z[i,bd,k] for k in range(1,len(block_durations)*nd*block_durations[-1])) == gb.quicksum(gb.quicksum(y[ab_id[0]][ab_id[1]][i,ab_id[2]] for ab_id in all_blocks[j]) for j in range(bd, len(block_durations))) )

# every surgeon can be assigned 1 block a day, every patient can be operated only once
for d in range(nd):
    model.addConstrs(gb.quicksum(gb.quicksum(y[d][r][i, b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) <= 1 for i in range(ns))
model.addConstrs(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j, b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)) <= 1 for j in range(np))

# surgeon's unavailability
if len(unavailability) > 0:
    for i in range(ns):
        model.addConstrs(gb.quicksum(gb.quicksum(y[d][r][i,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) == 0 for d in unavailability[i] )

for d in range(nd):
    for r in range(nr):
        # manage conflicting blocks (same time, same OR)
        for k in range(len(ors[d][r].conflicts)):
            if len(ors[d][r].conflicts[k])==0:
                continue
            model.addConstr(gb.quicksum(gb.quicksum(y[d][r][i,b] for i in range(ns)) for b in ors[d][r].conflicts[k])==1)
        for b in range(len(ors[d][r].blocks)):
            # patient can be operated only in the blocks assigned to their surgeon
            # model.addConstrs(gb.quicksum(x[d][r][pat.id,b] for pat in surgeons[i].patients) <= bigM*y[d][r][i, b] for i in range(ns))
            # block duration cannot be exceeded
            # model.addConstr(gb.quicksum(x[d][r][j,b] * patients[j].duration for j in range(np)) <= ors[d][r].blocks[b].duration)

            # capacity of the block b assigned to surgeon s cannot be exceeded
            model.addConstrs(
                gb.quicksum(x[d][r][pat.id, b] * pat.duration for pat in surgeons[i].patients) <= ors[d][r].blocks[
                    b].duration * y[d][r][i, b] for i in range(ns))

capacity = 0
for d in range(nd):
    for r in range(nr):
        capacity += ors[d][r].end - ors[d][r].start
alpha = 1
beta = 1
model.setObjective(alpha*(capacity - gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j,b]*patients[j].duration for j in range(np)) for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd))) +
                   beta*(gb.quicksum((1-gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)))*patients[j].duration*patients[j].master_priority for j in range(np))), gb.GRB.MINIMIZE)
# gb.quicksum(gb.quicksum(gb.quicksum(ors[d][r].blocks[b].duration*gb.quicksum(y[d][r][i,b] for i in range(ns))
# for i in range(ns):
#     model.setObjectiveN(gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][pat.id,b]*patients[pat.id].priority for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)) for pat in surgeons[i].patients), i+1, 1, -1)
# model.setParam(gb.GRB.Param.ObjNumber, 0)
# model.ObjNPriority = 2

s = time.time()
# model.optimize(callback_lazy)
model.optimize()
print('optimization time: ', time.time()-s)
print('objective: ', model.objVal)
for d in range(nd):
    print('DAY: ', d)
    for r in range(nr):
        print(' OR: ', r, ors[d][r].start, ors[d][r].end)
        for b in range(len(ors[d][r].blocks)):
            for i in range(ns):
                if y[d][r][i,b].x > 0.5:
                    print('     surgeon: ', i, 'start: ', ors[d][r].blocks[b].start, 'end: ', ors[d][r].blocks[b].end)
                    print('         patients: ', end='')
                    for pat in surgeons[i].patients:
                        if x[d][r][pat.id,b].x > 0.5:
                            print(pat.id, '(', pat.duration, ')', end='; ')
                    print('')

print('unscheduled: ')
for j in range(np):
    pat_scheduled = 0
    for d in range(nd):
        for r in range(nr):
            for b in range(len(ors[d][r].blocks)):
                pat_scheduled += round(x[d][r][j, b].x)
    if pat_scheduled == 0:
        print('{} {} {} {}'.format(patients[j].id, patients[j].surgeon, patients[j].priority, patients[j].duration))

print('# callbacks: ', ncallbacks)
print('# LC: ', n_lc)
print(capacity)