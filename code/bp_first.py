import gurobipy as grb
from gurobipy import GRB
import numpy as np
from parse_instance import parse_in
import time


def dual_lp_master_problem(ors, patients, alpha, beta):
    nd = len(ors)
    nr = len(ors[0])
    ns = len(surgeons)

    m = grb.Model()
    m._alpha = alpha
    m._beta = beta
    m._patients = patients
    m._ns = ns

    blocks = []

    for d in range(nd):
        for r in range(nr):
            for b in ors[d][r].blocks:
                blocks.append(b.duration)

    print(blocks)
    blocks = np.array(blocks)
    num_blocks = blocks.shape[0]
    m._blocks = blocks

    i = 0
    conflicts = []
    n_overlap = 0
    for d in range(nd):
        c_line = np.zeros(num_blocks)
        for r in range(nr):
            n_overlap += len(ors[d][r].conflicts)
            c_line[i:i+len(ors[d][r].blocks)] = 1
            i = i+len(ors[d][r].blocks)
        conflicts.append(c_line)

    conflicts.append(np.ones(num_blocks))
    conflicts = np.array(conflicts)
    m._conflicts = conflicts
    m._demands = np.ones(conflicts.shape[0])
    m._demands[-1] = 5  # dem_max

    pats = []
    for s in range(ns):
        pats_s = []
        for p in surgeons[s].patients:
            pats_s.append([p.duration, p.priority, p.priority])
        pats_s = np.array(pats_s)
        pats.append(pats_s)
    patients = pats

    i = 0
    cdrt = []
    for d in range(nd):
        for r in range(nr):
            for c in ors[d][r].conflicts:
                if len(c)>0:
                    c_line = np.zeros(blocks.shape[0])
                    c_line[i+np.array(c)] = 1
                    cdrt.append(c_line)
            i += len(ors[d][r].blocks)
    cdrt = np.array(cdrt)
    m._cdrt = cdrt

    m._patients = patients
    m._lambdas = m.addVars(cdrt.shape[0], ub=0.0, vtype=GRB.INTEGER)
    m._mus = m.addVars(ns, lb=0, vtype=GRB.INTEGER)
    m._w = []
    m._delta = []
    m._s = []
    m._a = []

    for s in range(ns):
        w = np.matmul(patients[s][:,0], patients[s][:,1])
        m.addConstr(m._mus[s] <= beta*w, name="first")
        m._w.append(w)
        m._delta.append(0)
        m._s.append(s)
        m._a.append(np.zeros(num_blocks))

    m.setObjective( sum(m._lambdas[i] for i in range(m._cdrt.shape[0])) + sum(m._mus[s] for s in range(m._ns) ), GRB.MAXIMIZE)
    m.Params.lazyConstraints = 1
    m._numblocks = num_blocks
    m.optimize(MP_callback)
    return m

def primal_problem(w, delta, a, alpha, beta, vtype, cdrt):
    ns = len(w)

    m = grb.Model()

    m._thetas = []
    for s in range(ns):
        ths = m.addVars(len(w[s]), lb=0,  vtype=vtype)
        m._thetas.append(ths)

    for i, c in enumerate(cdrt):
        m.addConstr(sum(sum(m._thetas[s][k] * np.matmul(c, a[s][k]) for k in range(len(w[s]))) for s in range(ns)) <= 1, name='lambda'+str(i))

    for s in range(ns):
        m.addConstr(sum(m._thetas[s][k] for k in range(len(w[s]))) >= 1, name='mu'+str(s))

    m._first = m.addVar(vtype=vtype)
    m._second = m.addVar(vtype=vtype)
    m.addConstr(m._first <= grb.quicksum(grb.quicksum(m._thetas[s][k] * alpha*delta[s][k] for k in range(len(w[s]))) for s in range(ns)))
    m.addConstr(m._second >= grb.quicksum(grb.quicksum(m._thetas[s][k] * beta*w[s][k]  for k in range(len(w[s]))) for s in range(ns)))

    # m.setObjective(sum(sum(m._thetas[s][k] * (beta*w[s][k] - alpha*delta[s][k]) for k in range(len(w[s]))) for s in range(ns)), GRB.MINIMIZE)
    m.setObjective(m._second-m._first, grb.GRB.MINIMIZE)
    m.setParam('OutputFlag', False)
    m.optimize()
    return m

def dual_problem(w, delta, a, alpha, beta, vtype, cdrt):
    ns = len(w)

    m = grb.Model()

    m._lambdas = m.addVars(cdrt.shape[0], ub=0, vtype=vtype, name='lambda')
    m._mus = m.addVars(ns, lb=0, vtype=vtype, name='mu')

    for s in range(ns):
        for k in range(len(w[s])):
            m.addConstr(sum(np.matmul(a[s][k], cdrt[i]) * m._lambdas[i] for i in range(cdrt.shape[0])) + m._mus[s] <= beta*w[s][k] - alpha*delta[s][k], name=str(s)+'_'+str(k))

    m.setObjective(sum(m._lambdas[i] for i in range(cdrt.shape[0])) + sum(m._mus[s] for s in range(ns)), GRB.MAXIMIZE)
    m.optimize()
    return m


def MP_callback(model, where):
    if where == grb.GRB.Callback.MIPSOL:
        lambdas = model.cbGetSolution(model._lambdas)
        for s in range(model._ns):
            shadow, a, w, delta = surgeon_subproblem(model._patients[s], model._blocks, lambdas, model._conflicts, model._demands, model._alpha, model._beta, model._cdrt)
            print('here', shadow,a)
            if shadow > 0:
                model.cbLazy(sum(model._lambdas[i] * np.matmul(a, model._conflicts[i]) for i in range(model._conflicts.shape[0])) + model._mus[s] <= model._beta*w - model._alpha*delta)#, name=str(len(model._w))+'lazy')
                model._w.append(w)
                model._delta.append(delta)
                model._s.append(s)
                model._a.append(a)
                break

def surgeon_subproblem(patients, blocks, lambdas, conflicts, demands, alpha, beta, cdrt):
    # patients = [(duration, priority inner, priority outer)]
    # blocks = array of lengths
    # conflicts = matrix, set of conflicting blocks in each line marked by 1
    # demands = max number of blocks assigned from each conflict
    m = grb.Model()

    o = m.addMVar(shape=blocks.shape[0], lb=0, vtype=GRB.INTEGER)
    x = m.addMVar(shape=(patients.shape[0], blocks.shape[0]), lb=0, vtype=GRB.INTEGER)

    m.addConstr(lhs=conflicts @ o <= demands)
    m.addConstrs(x[i,:] @ np.ones(blocks.shape[0]) <= 1 for i in range(patients.shape[0]))
    m.addConstrs(x[:,j] @ patients[:,0] <= o[j]*blocks[j] for j in range(blocks.shape[0]))

    print(conflicts[0,0], lambdas[0])
    m.setObjective( sum(lambdas[i]*(cdrt[i,:] @ o) for i in range(conflicts.shape[0])) - beta*np.matmul(patients[:,0],patients[:,2]) + beta*sum((patients[:,0]*patients[:,2]) @ x[:,j] for j in range(blocks.shape[0]))  + alpha*sum(patients[:,0] @ x[:,j] for j in range(blocks.shape[0])), GRB.MAXIMIZE)
    # m.Params.lazyConstraints = 1
    m._blocks = blocks
    m._o = o
    m._patients = patients
    m.optimize()

    ms = grb.Model()
    xs = ms.addMVar(shape=(patients.shape[0], blocks.shape[0]), vtype=GRB.INTEGER)
    os = np.array(o.X)

    ms.addConstrs(xs[i, :] @ np.ones(blocks.shape[0]) <= 1 for i in range(patients.shape[0]))
    ms.addConstrs(xs[:, j] @ patients[:, 0] <= os[j] * blocks[j] for j in range(blocks.shape[0]))
    ms.setObjective(sum(patients[:,1] @ xs[:,j]  for j in range(blocks.shape[0])), GRB.MAXIMIZE)
    ms.optimize()

    xpb = np.array(xs.X)

    w = np.matmul(patients[:,0]*patients[:,2], 1-np.matmul(xpb, np.ones(blocks.shape[0])))
    delta = np.matmul(patients[:,0], np.matmul(xpb, np.ones(blocks.shape[0])))
    c = sum(lambdas[i]*np.matmul(conflicts[i], os) for i in range(conflicts.shape[0]))
    shadow = c - beta*w + alpha*delta
    return shadow, os, w, delta

def surgeon_subproblem_LC(patients, blocks, lambdas, conflicts, demands, alpha, beta, cdrt):
    # patients = [(duration, priority inner, priority outer)]
    # blocks = array of lengths
    # conflicts = matrix, set of conflicting blocks in each line marked by 1
    # demands = max number of blocks assigned from each conflict
    m = grb.Model()

    o = m.addVars(blocks.shape[0], lb=0, vtype=GRB.INTEGER)
    x = m.addVars(patients.shape[0], blocks.shape[0], lb=0, vtype=GRB.INTEGER)

    m.addConstrs(sum(conflicts[c,b] * o[b] for b in range(blocks.shape[0])) <= demands[c] for c in range(conflicts.shape[0]))
    m.addConstrs(sum(x[i,b] for b in range(blocks.shape[0])) <= 1 for i in range(patients.shape[0]))
    m.addConstrs(sum(x[p,j] * patients[p,0] for p in range(patients.shape[0])) <= o[j]*blocks[j] for j in range(blocks.shape[0]))

    w = m.addVar(vtype=GRB.INTEGER)
    m.addConstr(w >= np.matmul(patients[:,0],patients[:,2]) - sum(sum(patients[p,0]*patients[p,2] * x[p,j] for p in range(patients.shape[0])) for j in range(blocks.shape[0])))
    delta = m.addVar(vtype=GRB.INTEGER)
    m.addConstr(delta <= sum(sum(patients[p,0] * x[p,j] for p in range(patients.shape[0])) for j in range(blocks.shape[0])))

    m.setObjective( sum(lambdas[i]*sum(cdrt[i,b] * o[b] for b in range(blocks.shape[0])) for i in range(cdrt.shape[0])) - beta*w  + alpha*delta, GRB.MAXIMIZE)
    m.Params.lazyConstraints = 1
    m._blocks = blocks
    m._o = o
    m._x = x
    m._patients = patients
    m.optimize(surgeon_callback)

    os = np.array([o[i].x for i in range(blocks.shape[0])])

    return m.objVal, os, w.x, delta.x


def surgeon_callback(model, where):
    if where == grb.GRB.Callback.MIPSOL:
        m = grb.Model()
        x = m.addVars(model._patients.shape[0], model._blocks.shape[0], lb=0, vtype=GRB.INTEGER)
        o = model.cbGetSolution(model._o)
        xp = model.cbGetSolution(model._x)
        m.addConstrs(sum(x[i, b] for b in range(model._blocks.shape[0])) <= 1 for i in range(model._patients.shape[0]))
        m.addConstrs(sum(x[p, j] * model._patients[p, 0] for p in range(model._patients.shape[0])) <= o[j] * blocks[j] for j in range(model._blocks.shape[0]))
        m.setObjective(sum(sum(model._patients[p, 1] * x[p, j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0])), GRB.MAXIMIZE)
        m.optimize()

        fs = 0
        for p in range(model._patients.shape[0]):
            for b in range(model._blocks.shape[0]):
                fs += xp[p,b] * model._patients[p,1]

        if m.objVal > fs:
            model.cbLazy( (sum(o[b] - o[b] * model._o[b] for b in range(model._blocks.shape[0])))*100000 + sum(sum( model._patients[p,1] * model._x[p,b] for b in range(model._blocks.shape[0])) for p in range(model._patients.shape[0])) >= m.objVal )


def surgeon_subproblem_LCz(patients, blocks, lambdas, conflicts, demands, alpha, beta, cdrt):
    # patients = [(duration, priority inner, priority outer)]
    # blocks = array of lengths
    # conflicts = matrix, set of conflicting blocks in each line marked by 1
    # demands = max number of blocks assigned from each conflict

    lc_version = 2

    all_blocks = []
    block_durations = []
    for i in range(len(end_times)):
        all_blocks.append([])
        block_durations.append(end_times[i] - start_times[0])
    for b in range(blocks.shape[0]):
        ind = block_durations.index(blocks[b])
        all_blocks[ind].append(b)

    m = grb.Model()

    o = m.addVars(blocks.shape[0], lb=0, vtype=GRB.INTEGER)
    x = m.addVars(patients.shape[0], blocks.shape[0], lb=0, vtype=GRB.INTEGER)

    m.addConstrs(sum(conflicts[c,b] * o[b] for b in range(blocks.shape[0])) <= demands[c] for c in range(conflicts.shape[0]))
    m.addConstrs(sum(x[i,b] for b in range(blocks.shape[0])) <= 1 for i in range(patients.shape[0]))
    m.addConstrs(sum(x[p,j] * patients[p,0] for p in range(patients.shape[0])) <= o[j]*blocks[j] for j in range(blocks.shape[0]))

    # manage variables 'z'
    max_z = len(block_durations) * nd * block_durations[-1]  # maximum number of assigned blocks*max capacity to 1 surgeon
    z = m.addVars(len(block_durations), max_z, vtype=grb.GRB.BINARY)
    for l in range(len(block_durations)):
        m.addConstrs(z[l, k] >= z[l, k + 1] for k in range(len(block_durations) * block_durations[-1] * nd - 1))

    for bd in range(len(block_durations)):
        m.addConstr(z[bd, 0] == 1)
        if lc_version == 0:  ## orig
            m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(o[ab_id] for ab_id in all_blocks[bd]))
        elif lc_version == 1:  ## 1. redefinition
            m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) for j in
                range(bd, len(block_durations))))
        else:  ## 2. redefinition
            m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) * block_durations[j] for
                j in range(bd, len(block_durations))))

    w = m.addVar(vtype=GRB.INTEGER)
    m.addConstr(w >= np.matmul(patients[:,0],patients[:,2]) - sum(sum(patients[p,0]*patients[p,2] * x[p,j] for p in range(patients.shape[0])) for j in range(blocks.shape[0])))
    delta = m.addVar(vtype=GRB.INTEGER)
    m.addConstr(delta <= sum(sum(patients[p,0] * x[p,j] for p in range(patients.shape[0])) for j in range(blocks.shape[0])))

    m.setObjective( sum(lambdas[i]*sum(cdrt[i,b] * o[b] for b in range(blocks.shape[0])) for i in range(cdrt.shape[0])) - beta*w  + alpha*delta, GRB.MAXIMIZE)
    m.Params.lazyConstraints = 1
    m._blocks = blocks
    m._o = o
    m._x = x
    m._z = z
    m._block_durations = block_durations
    m._max_z = max_z
    m._patients = patients
    m.setParam('OutputFlag', False)
    m.optimize(surgeon_callbackz)

    os = np.array([o[i].x for i in range(blocks.shape[0])])

    return m.objVal, os, w.x, delta.x


def surgeon_callbackz(model, where):
    bigM = 1000
    if where == grb.GRB.Callback.MIPSOL:
        m = grb.Model()
        x = m.addVars(model._patients.shape[0], model._blocks.shape[0], lb=0, vtype=GRB.INTEGER)
        o = model.cbGetSolution(model._o)
        xp = model.cbGetSolution(model._x)
        m.addConstrs(sum(x[i, b] for b in range(model._blocks.shape[0])) <= 1 for i in range(model._patients.shape[0]))
        m.addConstrs(sum(x[p, j] * model._patients[p, 0] for p in range(model._patients.shape[0])) <= o[j] * blocks[j] for j in range(model._blocks.shape[0]))
        m.setObjective(sum(sum(model._patients[p, 1] * x[p, j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0])), GRB.MAXIMIZE)
        m.setParam('OutputFlag', False)
        m.optimize()

        fs = 0
        for p in range(model._patients.shape[0]):
            for b in range(model._blocks.shape[0]):
                fs += xp[p,b] * model._patients[p,1]

        if m.objVal > fs:
            z_count = []
            for j in range(len(model._block_durations)):
                z_temp = 0
                for k in range(model._max_z):
                    z_temp += round(model.cbGetSolution(model._z[j, k]))
                z_count.append(z_temp - 1)

            fs = round(m.objVal)
            model.cbLazy(grb.quicksum(grb.quicksum(model._x[p,b] for b in range(model._blocks.shape[0])) * model._patients[p,1] for p in range(model._patients.shape[0]))
                         + bigM * (len(model._block_durations) - grb.quicksum(model._z[bd, z_count[bd]] for bd in range(len(model._block_durations)))) >= fs)



if __name__=="__main__":
    ns, npatients, nr, nd, surgeons, patients, ors, start_times, end_times, unavailability = parse_in('../data/1_RealLifeSurgeryTypesDatabase/constant_priority/51_ordays_5_load_1,05surgeons_2.txt')
    # mp = dual_lp_master_problem(ors, patients, 1, 1)
    # mp.computeIIS()
    # mp.write('51_iis.ilp')
    # print(mp._s)
    # print(mp._w)
    # print(mp._delta)
    # print(mp._a)

    nd = len(ors)
    nr = len(ors[0])
    ns = len(surgeons)

    blocks = []

    for d in range(nd):
        for r in range(nr):
            for b in ors[d][r].blocks:
                blocks.append(b.duration)
    blocks = np.array(blocks)

    conflicts = []
    i = 0
    for d in range(nd):
        c_line = np.zeros(blocks.shape[0])
        for r in range(nr):
            c_line[i:i+len(ors[d][r].blocks)] = 1
            i = i+len(ors[d][r].blocks)
        conflicts.append(c_line)
    conflicts.append(np.ones(blocks.shape[0]))
    conflicts = np.array(conflicts)
    demands = np.ones(conflicts.shape[0])
    demands[-1] = 5  # dem_max

    pats = []
    for s in range(ns):
        pats_s = []
        for p in surgeons[s].patients:
            pats_s.append([p.duration, p.priority, p.priority])
        pats_s = np.array(pats_s)
        pats.append(pats_s)
    patients = pats

    i = 0
    cdrt = []
    for d in range(nd):
        for r in range(nr):
            for c in ors[d][r].conflicts:
                if len(c)>0:
                    c_line = np.zeros(blocks.shape[0])
                    c_line[i+np.array(c)] = 1
                    cdrt.append(c_line)
            i += len(ors[d][r].blocks)
    cdrt = np.array(cdrt)

    w = []
    delta = []
    a = []

    for s in range(ns):
        ws = np.matmul(patients[s][:,0], patients[s][:,1])
        w.append([ws])
        delta.append([0])
        a.append([np.zeros(blocks.shape[0])])

    sh = 1

    t = time.time()

    mp = primal_problem(w, delta, a, 1,1, GRB.CONTINUOUS, cdrt)
    nc = 0
    while sh>0:
        sh = 0
        print(mp.objVal)
        lambdas = np.zeros(len(cdrt))
        for i, c in enumerate(cdrt):
            con = mp.getConstrByName('lambda'+str(i))
            lambdas[i] = con.Pi
            # lambdas[i] = mp.getAttr('Pi', mp.getConstrs())
        mus = np.zeros(ns)
        for s in range(ns):
            con = mp.getConstrByName('mu'+str(s))
            mus[s] = con.Pi
            # mus[s] = mp.getAttr('Pi', mp.getConstrs())

        for s in range(ns):
            shadow, os, ws, deltas = surgeon_subproblem_LCz(patients[s], blocks, lambdas, conflicts, demands, 1, 1, cdrt)

            if shadow + mus[s] >= 0:
                sh = 1
                nc += 1
                # print(os)
                # print(blocks)
                # print(patients[s][:,0])
                # print(patients[s][:,1])
                # print(ws, deltas)

                a[s].append(os)
                w[s].append(ws)
                delta[s].append(deltas)
                print(shadow, mus[s], nc)
                # mp = primal_problem(w, delta, a, 1,1, GRB.CONTINUOUS, cdrt)
                # break
        mp = primal_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, cdrt)

    capacity = 0
    for d in range(nd):
        for r in range(nr):
            capacity += ors[d][r].end - ors[d][r].start

    print(capacity - mp._first.x, mp._second.x)

    for s in range(ns):
        sh = 0
        for k,o in enumerate(a[s]):
            sh += mp._thetas[s][k].x*np.sum(o*blocks)
            print(s, sh)

    mp = primal_problem(w, delta, a, 1,1, GRB.INTEGER, cdrt)
    for s in range(ns):
        sh = 0
        for k,o in enumerate(a[s]):
            sh += mp._thetas[s][k].x*np.sum(o*blocks)
            print(s, mp._thetas[s][k].x, w[s][k], delta[s][k], o*blocks, sh)
    #
    #
    # print(capacity-mp._first.x)
    # print(mp._second.x)
    # print(mp.objVal)
    print(time.time() - t)
    print(nc)

    # mp = dual_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, cdrt)
    # while shadow > 0:
    #     try:
    #         print(mp.objVal)
    #     except:
    #         break
    #     lambdas = []
    #     for t in range(cdrt.shape[0]):
    #         lambdas.append(mp._lambdas[t].x)
    #     lambdas = np.array(lambdas)
    #     for s in range(ns):
    #         shadow, os, ws, deltas = surgeon_subproblem(patients[s], blocks, lambdas, conflicts, demands, 1, 1, cdrt)
    #         if shadow>0:
    #             print(os)
    #             print(blocks)
    #             print(patients[s][:,0])
    #             print(patients[s][:,1])
    #             print(ws, deltas)
    #
    #             a[s].append(os)
    #             w[s].append(ws)
    #             delta[s].append(deltas)
    #             mp = dual_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, cdrt)
    #             break
    # mp.computeIIS()
    # mp.write('51_iis.ilp')
    # print(a[0])