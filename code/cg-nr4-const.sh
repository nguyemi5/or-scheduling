#!/bin/bash
#SBATCH --job-name=cg-nr4-const
#SBATCH --output=/home/nguyemi5/or-scheduling/out/cg-nr4-const.out
#SBATCH --cpus-per-task=10
#SBATCH --mem=8G
#SBATCH --time=0-13:00:00
#SBATCH --mail-user=nguyemi5@fel.cvut.cz
#SBATCH --mail-type=ALL

module load Python/3.7.2-GCCcore-8.2.0
module load Gurobi/9.0.0

pip3 install numpy --user

cd /home/nguyemi5/or-scheduling/code/
python3 batch_columngen.py 20 0 4 constant_priority 1000
python3 batch_columngen.py 20 1 4 constant_priority 1000
python3 batch_columngen.py 20 2 4 constant_priority 1000

