#!/bin/bash
#SBATCH --job-name=cgs5
#SBATCH --output=/home/nguyemi5/or-scheduling/out/cgs5.out
#SBATCH --cpus-per-task=10
#SBATCH --mem=8G
#SBATCH --time=0-13:00:00
#SBATCH --mail-user=nguyemi5@fel.cvut.cz
#SBATCH --mail-type=ALL

module load Python/3.7.2-GCCcore-8.2.0
module load Gurobi/9.0.0

pip3 install numpy --user

cd /home/nguyemi5/or-scheduling/code/
python3 batch_columngen.py 5 1 1 constant_priority 500 0
python3 batch_columngen.py 5 1 1 random_priority 500 0
python3 batch_columngen.py 5 1 1 constant_priority 500 1
python3 batch_columngen.py 5 1 1 random_priority 500 1

python3 batch_columngen.py 5 2 1 constant_priority 500 0
python3 batch_columngen.py 5 2 1 random_priority 500 0
python3 batch_columngen.py 5 2 1 constant_priority 500 1
python3 batch_columngen.py 5 2 1 random_priority 500 1
