import sys
from column_gen import solve_instance
import datetime

ranges5 = [(51,55, '1,05'), (61, 65, '1,1'), (71,75, '1,15'), (81,85, '1,2')]
ranges10 = [(141,145, '1,05'), (151, 155, '1,1'), (161,165, '1,15'), (171,175, '1,2')]
ranges20 = [(321,325, '1,05'), (331, 335, '1,1'), (341,345, '1,15'), (351,355, '1,2')]
ranges40 = [(681,690, '1,05'), (691, 700, '1,1'), (701,710, '1,15'), (711,720, '1,2')]

dict_ranges = {5 : ranges5, 10:ranges10, 20:ranges20, 40:ranges40}

lc_version = int(sys.argv[2])      # 0 = original 1 = 1st redefinition, 2 = 2nd redefinition
alpha = 1
beta = 1
surgs = int(sys.argv[1])
rooms = int(sys.argv[3])
days = 5
timeout = int(sys.argv[5])    # in seconds
tolerance = 1e-3
recordLC = bool(int(sys.argv[6]))

priority = sys.argv[4]
data_dir = '../data/1_RealLifeSurgeryTypesDatabase/' + priority + '/'
filename = '../out/' + priority + '/exp' + str(datetime.datetime.now()) + '.txt'
out_log = open(filename, 'a')
out_log.write('### EXPERIMENT CHARACTERISTICS\n LC version: {}\n OR days: {}\n rooms: {}\n surgeons: {}\n alpha: {}, beta: {}\n timeout: {}\n record LC: {}\n'.format(lc_version, days, rooms, surgs, alpha, beta, timeout, recordLC))
out_log.write(' data directory: {}\n'.format(data_dir))
out_log.write('\n### RESULTS \n')
out_log.write('ID\t load\t runtime\t LB\t UB\t #added_columns\t #iterations\t time in SP\t time in MP\t time in CB\t #callbacks\t #added LC\n')
out_log.close()

for ran in dict_ranges[rooms*days]:
    for inst in range(ran[0], ran[1]+1):
        load = ran[2]
        ## setup options
        filename_data = data_dir + str(inst) + '_ordays_' + str(days*rooms) + '_load_' + load + 'surgeons_' + str(surgs) + '.txt'
        # t, rfirst, rsecond, ifirst, isecond, nc, nit, sub_time, mp_time, cb_time, ncb, nLC, dup = solve_instance(filename_data, lc_version, tolerance, timeout, alpha, beta, recordLC)
        t, lb, ub, nc, nit, sub_time, mp_time, cb_time, ncb, nLC, dup = solve_instance(filename_data, lc_version, tolerance, timeout, alpha, beta, recordLC)
        out_log = open(filename, 'a')
        if t<timeout:
            out_log.write('{}\t {}\t {:4.3f}\t {:3.2f}\t {}\t {}\t {}\t {:4.3f}\t {:4.3f}\t {:4.3f}\t {}\t {}\n'.format(inst, load, t, lb, ub, nc, nit, sub_time, mp_time, cb_time, ncb, nLC))
        else:
            out_log.write('{}\t {}\t timeout\t {:3.2f}\t {}\t {}\t {}\t {:4.3f}\t {:4.3f}\t {:4.3f}\t {}\t {} \n'.format(inst, load, lb, ub, nc, nit, sub_time, mp_time, cb_time, ncb, nLC))
        out_log.close()
