import random
import math

inp_filename = 'choir_1.txt'
restriction = 0
ns = 5
nd = 5
nr = 1

out_filename = 'in_choir'+str(ns)+'s'+str(nd)+'d'+str(nr)+'r_'+str(restriction)+'__.txt'

f = open(inp_filename)
lines = f.read().splitlines()
f.close()

surgeries = []
for line in lines[10:]:
    if len(line)>1:
        surgeries.append(list(map(float, line.split('\t'))))

np = len(surgeries)

# start_times = ['0', '120', '240', '360']
# end_times = ['120', '240', '360', '480']

start_times = ['0', '8', '16', '24']
end_times = ['8', '16', '24', '32']
priority_range = [1,1]

f = open(out_filename, 'w')
f.write('{} {} {} {}\n'.format(ns, np, nr, nd))

f.write('patients: ID surgeon priority duration\n')
for i in range(np):
    s = random.randint(0, ns-1)
    p = 1
    d = int(round((surgeries[i][3] + math.exp(surgeries[i][1] + (surgeries[i][2])**2/2))/15))
    f.write('{} {} {} {}\n'.format(i, s, p, d))

f.write('ORS: day OR start end\n')
capacity = 0
for d in range(nd):
    for r in range(nr):
        s = start_times[random.randint(0, restriction)]
        e = 32
        f.write('{} {} {} {}\n'.format(d, r, s, e))
        capacity += int(e)-int(s)

# f.write('Surgeon unavailability: id day\n')
# for s in range(ns):
#     f.write('{} {}\n'.format(s, random.randint(0, nd-1)))

f.write('start times:\n')
f.write(' '.join(start_times))
f.write('\n')

f.write('end times:\n')
f.write(' '.join(end_times))
f.write('\n')

max_capacity = nd*nr*(int(end_times[-1])-int(start_times[0]))
# f.write('OR capacity: '+str(capacity/max_capacity)+'\n')
print('OR capacity: '+str(capacity/max_capacity)+'\n')
f.close()