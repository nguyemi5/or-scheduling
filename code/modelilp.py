from instance import Surgeon, Patient, Block, OR
import gurobipy as gb
import random

ns = 10  # number of surgeons
np = 50  # number of patients
nr = 4  # number of ORs

start_times = [0, 8, 16, 24, 32]
end_times = [8, 16, 24, 32, 40]
priority_range = [1, 2]
duration_range = [3, 40]

surgeons = []
patients = []
blocks = []
ors = []

for i in range(ns):
    surgeons.append(Surgeon(i))

for i in range(np):
    s = random.randint(0, ns - 1)
    pat = Patient(i, s, random.randint(priority_range[0], priority_range[1]),
                  random.randint(duration_range[0], duration_range[1]))
    surgeons[s].patients.append(pat)
    patients.append(pat)

for i in range(nr):
    s = random.randint(0, len(start_times) - 1)
    e = random.randint(s, len(end_times) - 1)
    r = OR(i, start_times[s], end_times[e])
    ors.append(r)

for s in surgeons:
    print('surgeon: ', s.id)
    for p in s.patients:
        print('     patient: ', p.id, p.priority, p.duration)

for r in ors:
    print('OR: ', r.id, r.start, r.end)
    for b in r.blocks:
        print('     block: ', b.id, b.start, b.end)
    for c in r.conflicts:
        print('     conflicts: ', c)

model = gb.Model()
y = []
x = []
bigM = 1000

for r in range(nr):
    yr = model.addVars(ns, len(ors[r].blocks), vtype=gb.GRB.BINARY)
    xr = model.addVars(np, len(ors[r].blocks), vtype=gb.GRB.BINARY)
    y.append(yr)
    x.append(xr)

model.addConstrs(gb.quicksum(gb.quicksum(y[r][i, b] for b in range(len(ors[r].blocks))) for r in range(nr)) <= 1 for i in range(ns))
model.addConstrs(gb.quicksum(gb.quicksum(x[r][j, b] for b in range(len(ors[r].blocks))) for r in range(nr)) <= 1 for j in range(np))
for r in range(nr):
    for k in range(len(ors[r].conflicts)):
        if len(ors[r].conflicts[k])==0:
            continue
        model.addConstr(gb.quicksum(gb.quicksum(y[r][i,b] for i in range(ns)) for b in ors[r].conflicts[k])==1)
    for b in range(len(ors[r].blocks)):
        model.addConstrs(gb.quicksum(x[r][pat.id,b] for pat in surgeons[i].patients) <= bigM*y[r][i, b] for i in range(ns))
        model.addConstr(gb.quicksum(x[r][j,b] * patients[j].duration for j in range(np)) <= ors[r].blocks[b].duration)

##### add lazy constraints



model.setObjective( gb.quicksum(gb.quicksum(ors[r].blocks[b].duration*gb.quicksum(y[r][i,b] for i in range(ns)) - gb.quicksum(x[r][j,b]*patients[j].duration for j in range(np)) for b in range(len(ors[r].blocks))) for r in range(nr)), gb.GRB.MAXIMIZE)

model.optimize()

for r in range(nr):
    for b in range(len(ors[r].blocks)):
        for i in range(ns):
            if y[r][i,b].x == 1:
                print('surgeon: ', i, 'or: ', r, 'start: ', ors[r].blocks[b].start, 'end: ', ors[r].blocks[b].end)