import random

filename = 'in11.txt'
ns = 10     # number of surgeons
np = 100     # number of patients
nr = 2      # number of ORs
nd = 5      # number of days in the planning horizon - week=5

start_times = ['0', '8', '16', '24', '32']
end_times = ['8', '16', '24', '32', '40']
priority_range = [1, 3]
duration_range = [3, 16]

f = open(filename, 'w')
f.write('{} {} {} {}\n'.format(ns, np, nr, nd))

f.write('patients: ID surgeon priority duration\n')
for i in range(np):
    s = random.randint(0, ns-1)
    p = random.randint(priority_range[0], priority_range[1])
    d = random.randint(duration_range[0], duration_range[1])
    f.write('{} {} {} {}\n'.format(i, s, p, d))

f.write('ORS: day OR start end\n')
for d in range(nd):
    for r in range(nr):
        s = random.randint(0, len(start_times)-1)
        e = random.randint(s, len(end_times)-1)
        f.write('{} {} {} {}\n'.format(d, r, start_times[s], end_times[e]))

f.write('Surgeon unavailability: id day\n')
for s in range(ns):
    f.write('{} {}\n'.format(s, random.randint(0, nd-1)))

f.write('start times:\n')
f.write(' '.join(start_times))
f.write('\n')

f.write('end times:\n')
f.write(' '.join(end_times))
f.write('\n')

f.close()