import gurobipy as grb
from gurobipy import GRB
import numpy as np
from parse_instance import parse_in
import time

def primal_problem(w, delta, a, alpha, beta, vtype, cdrt, timeout):
    ns = len(w)

    m = grb.Model()

    m._thetas = []
    for s in range(ns):
        ths = m.addVars(len(w[s]), lb=0,  vtype=vtype)
        m._thetas.append(ths)

    for i, c in enumerate(cdrt):
        m.addConstr(sum(sum(m._thetas[s][k] * np.matmul(c, a[s][k]) for k in range(len(w[s]))) for s in range(ns)) <= 1, name='lambda'+str(i))

    for s in range(ns):
        m.addConstr(sum(m._thetas[s][k] for k in range(len(w[s]))) >= 1, name='mu'+str(s))

    m._first = m.addVar(vtype=vtype)
    m._second = m.addVar(vtype=vtype)
    m.addConstr(m._first <= grb.quicksum(grb.quicksum(m._thetas[s][k] * alpha*delta[s][k] for k in range(len(w[s]))) for s in range(ns)))
    m.addConstr(m._second >= grb.quicksum(grb.quicksum(m._thetas[s][k] * beta*w[s][k]  for k in range(len(w[s]))) for s in range(ns)))

    m.setObjective(m._second - m._first, grb.GRB.MINIMIZE)
    m.setParam('OutputFlag', False)
    m.Params.TimeLimit = timeout
    m.optimize()
    return m


def surgeon_subproblem(patients, blocks, lambdas, conflicts, demands, alpha, beta, cdrt, lc_version, all_blocks, block_durations, nd, timeout, mu, constrs, ncb ):
    # patients = [(duration, priority inner, priority outer)]
    # blocks = array of block lengths
    # conflicts =  matrix, set of blocks in each line marked by 1  - e.g. unavailability, max blocks per day
    # demands = max number of blocks assigned from each surgeon conflict
    # cdrt = matrix, set of conflicting blocks in each line marked by 1
    # all_blocks = block ids sorted by block lengths (for z variables)
    # block_durations = array of block durations
    # constrs = constraints on minimum surgeon objective: [number of assigned blocks for each length, min objective]


    bigM = 1000
    m = grb.Model()

    o = m.addVars(blocks.shape[0], vtype=GRB.BINARY) # surgeon blocks assignment
    x = m.addVars(patients.shape[0], blocks.shape[0], vtype=GRB.BINARY)  # patient block assignment

    m.addConstrs(sum(conflicts[c,b] * o[b] for b in range(blocks.shape[0])) <= demands[c] for c in range(conflicts.shape[0]))
    m.addConstrs(sum(x[i,b] for b in range(blocks.shape[0])) <= 1 for i in range(patients.shape[0]))
    m.addConstrs(sum(x[p,j] * patients[p,0] for p in range(patients.shape[0])) <= o[j]*blocks[j] for j in range(blocks.shape[0]))

    # manage variables 'z'
    max_z = len(block_durations) * nd * block_durations[-1]  # maximum number of assigned blocks*max capacity to 1 surgeon
    z = m.addVars(len(block_durations), max_z, vtype=grb.GRB.BINARY)
    for l in range(len(block_durations)):
        m.addConstrs(z[l, k] >= z[l, k + 1] for k in range(max_z-1))#len(block_durations) * block_durations[-1] * nd - 1))

    for bd in range(len(block_durations)):
        m.addConstr(z[bd, 0] == 1)
        if lc_version == 0:  ## orig
            m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(o[ab_id] for ab_id in all_blocks[bd]))
        elif lc_version == 1:  ## 1. redefinition
            m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) for j in
                range(bd, len(block_durations))))
        else:  ## 2. redefinition
            m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) * block_durations[j] for
                j in range(bd, len(block_durations))))

    for c in constrs:
        m.addConstr(grb.quicksum(grb.quicksum(x[p, b] for b in range(blocks.shape[0])) * patients[p, 1] for p in range(patients.shape[0]))
                    + bigM * (len(block_durations) - grb.quicksum(z[bd, c[bd]] for bd in range(len(block_durations)))) >= c[-1])

    m._w = m.addVar(vtype=GRB.CONTINUOUS)
    m.addConstr(m._w == np.matmul(patients[:,0],patients[:,2]) - sum(sum(patients[p,0]*patients[p,2] * x[p,j] for p in range(patients.shape[0])) for j in range(blocks.shape[0])))
    m._delta = m.addVar(vtype=GRB.CONTINUOUS)
    m.addConstr(m._delta == sum(sum(patients[p,0] * x[p,j] for p in range(patients.shape[0])) for j in range(blocks.shape[0])))

    m.setObjective( sum(lambdas[i]*sum(cdrt[i,b] * o[b] for b in range(blocks.shape[0])) for i in range(cdrt.shape[0])) - beta*m._w  + alpha*m._delta, GRB.MAXIMIZE)
    m.Params.lazyConstraints = 1
    m._blocks = blocks
    m._o = o
    m._x = x
    m._z = z
    m._block_durations = block_durations
    m._max_z = max_z
    m._patients = patients
    m.setParam('OutputFlag', False)
    m.Params.TimeLimit = timeout
    # m.setParam(GRB.Param.PoolSolutions, 5)
    # m.setParam(GRB.Param.PoolSearchMode, 2)
    m._scount = 0
    m._objs = []
    m._os = []
    m._ws = []
    m._deltas = []

    m._mu = mu
    m._lambdas = lambdas
    m._alpha = alpha
    m._beta = beta
    m._cdrt = cdrt
    m._constrs = constrs
    m._ncb = ncb
    m._optim = []
    m._cbtime = 0

    m.optimize(surgeon_callback)

    os = tuple([o[i].x for i in range(blocks.shape[0])])
    if m.objVal+mu>0 and os not in m._os:
        m._objs.append(m.objVal+mu)
        m._os.append(os)
        m._ws.append(m._w.x)
        m._deltas.append(m._delta.x)
        m._optim.append(0)

    return m._objs, m._os, m._ws, m._deltas, m._constrs, m._ncb, m._optim, m._cbtime


def surgeon_callback(model, where):
    bigM = 1000
    if where == grb.GRB.Callback.MIPSOL:
        st = time.time()
        model._ncb += 1
        m = grb.Model()
        x = m.addVars(model._patients.shape[0], model._blocks.shape[0], lb=0, vtype=GRB.BINARY)
        o = model.cbGetSolution(model._o)
        xp = model.cbGetSolution(model._x)

        for b in range(model._blocks.shape[0]):
            o[b] = round(o[b])
        # sp_obj = sum(model._lambdas[i] * sum(o[b] for b in model._cdrt[i]) for i in range(model._cdrt.shape[0]))  + model._mu

        m.addConstrs(sum(x[i, b] for b in range(model._blocks.shape[0])) <= 1 for i in range(model._patients.shape[0]))
        m.addConstrs(sum(x[p, j] * model._patients[p, 0] for p in range(model._patients.shape[0])) <= o[j] * model._blocks[j] for j in range(model._blocks.shape[0]))

        w = m.addVar(vtype=GRB.CONTINUOUS)
        m.addConstr(w == np.matmul(model._patients[:,0],model._patients[:,2]) - sum(sum(model._patients[p,0]*model._patients[p,2] * x[p,j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0])))
        delta = m.addVar(vtype=GRB.CONTINUOUS)
        m.addConstr(delta == sum(sum(model._patients[p,0] * x[p,j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0])))

        m.setObjective(sum(sum(model._patients[p, 1] * x[p, j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0])), GRB.MAXIMIZE)
                       # + smallM*model._alpha*delta - smallM*model._beta*w, GRB.MAXIMIZE)
        m.setParam('OutputFlag', False)
        m.optimize()

        if (m.getAttr(GRB.Attr.Status) != GRB.OPTIMAL):
            # print('Callback infeasible!!')
            return

        fs = 0
        for p in range(model._patients.shape[0]):
            for b in range(model._blocks.shape[0]):
                fs += round(xp[p,b]) * model._patients[p,1]

        if m.objVal > fs:# + smallM*model._alpha*model.cbGetSolution(model._delta) - smallM*model._alpha*model.cbGetSolution(model._w):
            print('LC added here')
            z_count = []
            for j in range(len(model._block_durations)):
                z_temp = 0
                for k in range(1,model._max_z):
                    z_temp += round(model.cbGetSolution(model._z[j, k]))
                z_count.append(z_temp)

            model.cbLazy(grb.quicksum(grb.quicksum(model._x[p,b] for b in range(model._blocks.shape[0])) * model._patients[p,1] for p in range(model._patients.shape[0]))
                         + bigM * (len(model._block_durations) - grb.quicksum(model._z[bd, z_count[bd]] for bd in range(len(model._block_durations)))) >= m.objVal)
            model._constrs.append(z_count+[m.objVal])

        model._cbtime += time.time()-st

def solve_instance(in_filename, lc_version, tolerance, timeout, alpha, beta, recordLC):
    ns, npatients, nr, nd, surgeons, patients, ors, start_times, end_times, unavailability = parse_in(in_filename)

    nd = len(ors)
    nr = len(ors[0])
    ns = len(surgeons)
    ncallbacks = 0

    blocks = []

    for d in range(nd):
        for r in range(nr):
            for b in ors[d][r].blocks:
                blocks.append(b.duration)
    blocks = np.array(blocks)

    all_blocks = []
    block_durations = []
    for i in range(len(end_times)):
        all_blocks.append([])
        block_durations.append(end_times[i] - start_times[0])
    for b in range(blocks.shape[0]):
        ind = block_durations.index(blocks[b])
        all_blocks[ind].append(b)

    conflicts = []
    i = 0
    for d in range(nd):
        c_line = np.zeros(blocks.shape[0])
        for r in range(nr):
            c_line[i:i + len(ors[d][r].blocks)] = 1
            i = i + len(ors[d][r].blocks)
        conflicts.append(c_line)
    conflicts.append(np.ones(blocks.shape[0]))
    conflicts = np.array(conflicts)
    demands = np.ones(conflicts.shape[0])
    demands[-1] = 5  # dem_max

    pats = []
    for s in range(ns):
        pats_s = []
        for p in surgeons[s].patients:
            pats_s.append([p.duration, p.priority, p.priority])
        pats_s = np.array(pats_s)
        pats.append(pats_s)
    patients = pats

    i = 0
    cdrt = []
    for d in range(nd):
        for r in range(nr):
            for c in ors[d][r].conflicts:
                if len(c) > 0:
                    c_line = np.zeros(blocks.shape[0])
                    c_line[i + np.array(c)] = 1
                    cdrt.append(c_line)
            i += len(ors[d][r].blocks)
    cdrt = np.array(cdrt)

    w = []
    delta = []
    a = []
    fs = []
    constrs = []

    for s in range(ns):
        if len(patients[s])==0:
            w.append([0])
        else:
            ws = np.matmul(patients[s][:, 0], patients[s][:, 1])
            w.append([ws])
        delta.append([0])
        a.append([[0]*blocks.shape[0]])
        fs.append([0])
        constrs.append([])

    ### 65: 2 surgeons, random priority
    # w[0].append(9)
    # w[1].append(13)
    # delta[0].append(sum(patients[0][:, 0]) - 9)
    # delta[1].append(sum(patients[1][:, 0]) - 13)
    # a[0].append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #              0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
    #              0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
    #              0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
    #              1, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    # a[1].append([0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
    #              0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
    #              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #              0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
    #              0, 0, 0, 0, 0, 0, 1, 0, 0, 0])
    # fs[0].append(0)
    # fs[1].append(0)

    sh = 1

    sub_time = 0
    mp_time = 0
    cb_time = 0

    t = time.time()
    mp = primal_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, cdrt, timeout)
    mp_time += time.time()-t
    nc = 0
    niterations = 0
    nLC = 0

    while sh > 0 and time.time()-t < timeout:
        niterations += 1
        sh = 0
        print(mp.objVal)
        # Pi = np.array(mp.getAttr('Pi', mp.getConstrs()))
        lambdas = len(cdrt)*[[0]]
        mus = ns*[[0]]
        for i in range(len(cdrt)):
            lambdas[i] = mp.getConstrByName('lambda'+str(i)).Pi
        for s in range(ns):
            mus[s] = mp.getConstrByName('mu'+str(s)).Pi
        # lambdas = Pi[:len(cdrt)]
        # mus = Pi[len(cdrt):len(cdrt) + ns]

        for s in range(ns):
            if len(patients[s])==0:
                continue
            st = time.time()
            if recordLC:
                shadows, os, ws, deltas, constrs[s], ncallbacks, optim, cbt = surgeon_subproblem(patients[s], blocks, lambdas,
                                                                                     conflicts, demands, alpha, beta,
                                                                                     cdrt, lc_version, all_blocks,
                                                                                     block_durations, nd, timeout,
                                                                                     mus[s], constrs[s], ncallbacks)
            else:
                shadows, os, ws, deltas, constrs[s], ncallbacks, optim, cbt = surgeon_subproblem(patients[s], blocks, lambdas,
                                                                                     conflicts, demands, alpha, beta,
                                                                                     cdrt, lc_version, all_blocks,
                                                                                     block_durations, nd, timeout,
                                                                                     mus[s], [], ncallbacks)
                nLC += len(constrs[s])
            sub_time += time.time()-st
            cb_time += cbt

            # if len(os) != len(set([tuple(os[k]) for k in range(len(os))])):
            #     print('adding duplicit columns: ', len(os), len(set([tuple(os[k]) for k in range(len(os))])))
            #     for i in range(len(ws)):
            #         print(ws[i], deltas[i], os[i])

            for i in range(len(ws)):
                # if os[i] in a[s]:
                #     ind = a[s].index(os[i])
                #     print(shadows[i], ': ', w[s][ind], '==', ws[i], ', ', delta[s][ind], '==', deltas[i], ', ',fs[s][ind], '==', optim[i])
                #     # sh = 0
                #     # break
                if shadows[i] > tolerance:
                    sh = 1
                    nc += 1
                    a[s].append(os[i])
                    w[s].append(ws[i])
                    delta[s].append(deltas[i])
                    fs[s].append(optim[i])
                    print(shadows[i], nc)
        st = time.time()
        mp = primal_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, cdrt, timeout)
        mp_time += time.time() - st
        # st = time.time()
        # mp = primal_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, cdrt, timeout)
        # mp_time += time.time() - st

    capacity = 0
    for d in range(nd):
        for r in range(nr):
            capacity += ors[d][r].end - ors[d][r].start

    relaxed_first = capacity - mp._first.x
    relaxed_second = mp._second.x

    for s in range(ns):
        print(s)
        th_s = 0
        w_s = 0
        delta_s = 0
        time_s = 0
        for k in range(len(w[s])):
            th_s += mp._thetas[s][k].x
            delta_s += mp._thetas[s][k].x*delta[s][k]
            w_s += mp._thetas[s][k].x*w[s][k]
            time_s += mp._thetas[s][k].x*np.matmul(a[s][k], blocks)
            if mp._thetas[s][k].x > 0:
                print(mp._thetas[s][k].x, w[s][k], delta[s][k], ':')
                bs = []
                for b in range(blocks.shape[0]):
                    if a[s][k][b]:
                        bs.append(blocks[b])
                print(bs)
                print(a[s][k])
        print(th_s, delta_s, w_s, time_s)

    mp = primal_problem(w, delta, a, 1, 1, GRB.INTEGER, cdrt, timeout)

    duplicit = 0
    for s in range(ns):
        duplicit += len(a[s]) - len(set([tuple(a[s][k]) for k in range(len(a[s]))]))

    if recordLC:
       nLC = sum(len(constrs[s]) for s in range(ns))

    print(w)
    print(delta)
    print(a)
    print(fs)
    # print(itnum)

    print(blocks)

    return time.time()-t, relaxed_first, relaxed_second, capacity-mp._first.x, mp._second.x, nc, niterations, sub_time, mp_time, cb_time, ncallbacks, nLC, duplicit

if __name__=="__main__":
    t, rfirst, rsecond, ifirst, isecond, nc, nit, sub_time, mp_time, cb_time, ncb, nLC, dup = solve_instance('../data/1_RealLifeSurgeryTypesDatabase/random_priority/65_ordays_5_load_1,1surgeons_2.txt', 0, 0.001, 200, 1, 1, 1)
    print(t, rfirst, rsecond, ifirst, isecond, nc, nit, sub_time, mp_time, cb_time, ncb, nLC, dup)
