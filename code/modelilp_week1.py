from parse_instance import parse_in
import gurobipy as gb
import copy
import time
import datetime
import sys

def callback_lazy(model, where):
    global ncallbacks, n_lc, cb_time # count real callbacks and added LC

    if where == gb.GRB.Callback.MIPSOL: # check if current solution is optimal
        ss = time.time()
        ncallbacks += 1
        # generate new lazy constraint for each surgeon
        for i in range(ns):
            ss = time.time()
            # extract blocks assigned to surgeon i
            assigned_blocks = []
            for d in range(nd):
                for r in range(nr):
                    for b in range(len(ors[d][r].blocks)):
                        if round(model.cbGetSolution(y[d][r][i,b])) == 1:
                            assigned_blocks.append(ors[d][r].blocks[b].duration)

            # initiate a model to calculate an optimal patient selection
            fmodel = gb.Model()
            xf = fmodel.addVars(len(surgeons[i].patients), len(assigned_blocks), vtype=gb.GRB.BINARY)
            # duration of assigned block cannot be exceeded
            fmodel.addConstrs(gb.quicksum(xf[j,b]*surgeons[i].patients[j].duration for j in range(len(surgeons[i].patients))) <= assigned_blocks[b] for b in range(len(assigned_blocks)))
            # each patient can be operated only once
            fmodel.addConstrs(gb.quicksum(xf[j,b] for b in range(len(assigned_blocks))) <= 1 for j in range(len(surgeons[i].patients)))
            # the objective is to operate the highest priority patients
            fmodel.setObjective(gb.quicksum(gb.quicksum(xf[j,b] for b in range(len(assigned_blocks)))*surgeons[i].patients[j].priority for j in range(len(surgeons[i].patients))), gb.GRB.MAXIMIZE)
            fmodel.optimize()
            # calculate value of assignment in the current solution
            fs = 0
            for pat in surgeons[i].patients:
                pat_scheduled = 0
                for d in range(nd):
                    for r in range(nr):
                        for b in range(len(ors[d][r].blocks)):
                            pat_scheduled += round(model.cbGetSolution(x[d][r][pat.id, b]))
                fs += pat_scheduled*pat.priority

            print('surgeon: ', i, ', fs: ', fs, ', opt: ', round(fmodel.objVal))
            # if the current solution is not optimal, add a lazy constraint
            if round(fmodel.objVal) > fs:
                n_lc += 1
                # count z in current assignment
                z_count = []
                for j in range(len(block_durations)):
                    z_temp = 0
                    for k in range(max_z):
                        z_temp += round(model.cbGetSolution(z[i,j,k]))
                    z_count.append(z_temp-1)

                fs = round(fmodel.objVal)
                model.cbLazy(gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][pat.id,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd))*pat.priority for pat in surgeons[i].patients)
                             + bigM*(len(block_durations) - gb.quicksum(z[i,bd,z_count[bd]] for bd in range(len(block_durations)))) >= fs )
                # break
        cb_time += time.time() - ss

ranges5 = [(51,55, '1,05'), (61, 65, '1,1'), (71,75, '1,15'), (81,85, '1,2')]
ranges10 = [(141,150, '1,05'), (151, 160, '1,1'), (161,170, '1,15'), (171,180, '1,2')]
ranges20 = [(321,323, '1,05'), (331, 333, '1,1'), (341,343, '1,15'), (351,353, '1,2')]
# ranges20 = [(351,353, '1,2')]
ranges40 = [(681,690, '1,05'), (691, 700, '1,1'), (701,710, '1,15'), (711,720, '1,2')]

dict_ranges = {5 : ranges5, 10:ranges10, 20:ranges20, 40:ranges40}

lc_version = int(sys.argv[2])      # 0 = original 1 = 1st redefinition, 2 = 2nd redefinition
alpha = 1
beta = 1
surgs = int(sys.argv[1])
rooms = int(sys.argv[3])
days = 5
timeout = int(sys.argv[5])    # in seconds

priority = sys.argv[4]
data_dir = '../data/1_RealLifeSurgeryTypesDatabase/' + priority + '/'
filename = '../out/ilp/' + priority + '/exp' + str(datetime.datetime.now()) + '.txt'
out_log = open(filename, 'a')
out_log.write('### EXPERIMENT CHARACTERISTICS\n LC version: {}\n OR days: {}\n rooms: {}\n surgeons: {}\n alpha: {}, beta: {}\n timeout: {}\n'.format(lc_version, days, rooms, surgs, alpha, beta, timeout))
out_log.write(' data directory: {}\n'.format(data_dir))
out_log.write('\n### RESULTS \n')
out_log.write('ID\t CPU_time\t time_in_cb\t #cb\t #added_LC\t first_obj\t second_obj\t load\n\n')
out_log.close()

for ran in dict_ranges[rooms*days]:
    for inst in range(ran[0], ran[1]+1):
        load = ran[2]
        ## setup options
        filename_data = data_dir + str(inst) + '_ordays_' + str(days*rooms) + '_load_' + load + 'surgeons_' + str(surgs) + '.txt'

        ## monitoring variables
        ncallbacks = 0
        n_lc = 0
        cb_time = 0

        ns, np, nr, nd, surgeons, patients, ors, start_times, end_times, unavailability = parse_in(filename_data)
        unavailability = []

        # sort blocks according to length (to manage variables 'z')
        all_blocks = []
        block_durations = []
        for i in range(len(end_times)):
            all_blocks.append([])
            block_durations.append(end_times[i]-start_times[0])
        for d in range(nd):
            for r in range(nr):
                for b in ors[d][r].blocks:
                    ind = block_durations.index(b.duration)
                    all_blocks[ind].append((d,r,b.id))

        model = gb.Model()
        model.Params.lazyConstraints = 1
        y = []
        x = []
        bigM = 1000

        for i in range(nd):
            yd = []
            xd = []
            for r in range(nr):
                yr = model.addVars(ns, len(ors[i][r].blocks), vtype=gb.GRB.BINARY)      # surgeon - block assignment
                xr = model.addVars(np, len(ors[i][r].blocks), vtype=gb.GRB.BINARY)      # patient - block assignment
                yd.append(yr)
                xd.append(xr)
            y.append(yd)
            x.append(xd)

        # manage variables 'z'
        max_z = len(block_durations) * nd * block_durations[-1]  # maximum number of assigned blocks*max capacity to 1 surgeon
        z = model.addVars(ns, len(block_durations), max_z, vtype=gb.GRB.BINARY)
        for i in range(ns):
            for l in range(len(block_durations)):
                model.addConstrs(z[i,l,k] >= z[i,l,k+1] for k in range(len(block_durations)*block_durations[-1]*nd-1))

        for i in range(ns):
            for bd in range(len(block_durations)):
                model.addConstr(z[i,bd,0] == 1)
                if lc_version == 0:   ## orig
                    model.addConstr(gb.quicksum(z[i, bd, k] for k in range(1, max_z)) == gb.quicksum(y[ab_id[0]][ab_id[1]][i, ab_id[2]] for ab_id in all_blocks[bd]))
                elif lc_version == 1:   ## 1. redefinition
                    model.addConstr(gb.quicksum(z[i, bd, k] for k in range(1, max_z)) == gb.quicksum(gb.quicksum(y[ab_id[0]][ab_id[1]][i, ab_id[2]] for ab_id in all_blocks[j]) for j in range(bd, len(block_durations))))
                else:   ## 2. redefinition
                    model.addConstr(gb.quicksum(z[i, bd, k] for k in range(1, max_z)) == gb.quicksum(gb.quicksum(y[ab_id[0]][ab_id[1]][i, ab_id[2]] for ab_id in all_blocks[j])*block_durations[j] for j in range(bd, len(block_durations))))

        # every surgeon can be assigned 1 block a day, every patient can be operated only once
        for d in range(nd):
            model.addConstrs(gb.quicksum(gb.quicksum(y[d][r][i, b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) <= 1 for i in range(ns))
        model.addConstrs(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j, b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)) <= 1 for j in range(np))

        # surgeon's unavailability
        if len(unavailability) > 0:
            for i in range(ns):
                model.addConstrs(gb.quicksum(gb.quicksum(y[d][r][i,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) == 0 for d in unavailability[i] )

        for d in range(nd):
            for r in range(nr):
                # manage conflicting blocks (same time, same OR)
                for k in range(len(ors[d][r].conflicts)):
                    if len(ors[d][r].conflicts[k])==0:
                        continue
                    model.addConstr(gb.quicksum(gb.quicksum(y[d][r][i,b] for i in range(ns)) for b in ors[d][r].conflicts[k])<=1)
                for b in range(len(ors[d][r].blocks)):
                    # capacity of the block b assigned to surgeon s cannot be exceeded
                    model.addConstrs(gb.quicksum(x[d][r][pat.id, b]*pat.duration for pat in surgeons[i].patients) <= ors[d][r].blocks[b].duration*y[d][r][i, b] for i in range(ns))
                    # patient can be operated only in the blocks assigned to their surgeon
                    # model.addConstrs(gb.quicksum(x[d][r][pat.id, b] for pat in surgeons[i].patients) <= bigM * y[d][r][i, b] for i in range(ns))
                    # block duration cannot be exceeded
                    # model.addConstr(gb.quicksum(x[d][r][j, b] * patients[j].duration for j in range(np)) <= ors[d][r].blocks[b].duration)

        capacity = 0
        for d in range(nd):
            for r in range(nr):
                capacity += ors[d][r].end - ors[d][r].start

        first_term = model.addVar(vtype=gb.GRB.INTEGER)
        second_term = model.addVar(vtype=gb.GRB.INTEGER)
        model.addConstr(first_term == gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j,b]*patients[j].duration for j in range(np)) for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)))
        model.addConstr(second_term == gb.quicksum((1-gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)))*patients[j].duration*patients[j].master_priority for j in range(np)))

        model.setObjective(alpha*(capacity-first_term) + beta*second_term, gb.GRB.MINIMIZE)

        model.Params.TimeLimit = timeout
        s = time.time()
        model.optimize(callback_lazy)
        cputime = time.time() - s

        out_log = open(filename, 'a')
        if cputime < timeout:
            out_log.write('{}\t {:2.3f}\t {:.3f}\t {}\t {}\t {:.0f}\t {:.0f}\t {}\t {}\n'.format(inst, cputime, cb_time, ncallbacks, n_lc, capacity-first_term.x, second_term.x, load, model.objVal))
        else:
            out_log.write('{}\t timeout \t {:2.2f}%\n'.format(inst, model.MIPGap*100))
        out_log.close()

out_log.close()