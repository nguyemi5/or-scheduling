import gurobipy as grb
from gurobipy import GRB
import numpy as np
from parse_instance import parse_in
import time
import copy

class Node:
    def __init__(self, ns, lb, ub, frac_s, frac_b, history):
        self.s = frac_s
        self.b = frac_b
        self.lb = lb
        self.ub = ub
        if history==None:
            self.history = []
            for s in range(ns):
                self.history.append([])
            self.ub = np.inf
            self.lb = 0
        else:
            self.history = history[:]

class Instance:
    def __init__(self, in_filename, lc_version, tolerance, timeout, alpha, beta, recordLC):
        self.smallM = 0.001
        self.bigM = 1000
        self.nodes = []
        self.optim = False
        self.filename = in_filename
        self.lc_version = lc_version
        self.tolerance = tolerance
        self.timeout = timeout
        self.alpha = alpha
        self.beta = beta
        self.recordLC = recordLC
        # self.rel_obj = np.inf
        # self.int_obj = np.inf
        # self.rel_lb = 0
        # self.int_lb = 0
        # self.rel_ub = np.inf
        # self.int_ub = np.inf
        self.numnodes = 0

        ns, npatients, nr, nd, surgeons, patients, ors, start_times, end_times, unavailability = parse_in(in_filename)
        self.ns = ns
        self.npatients = npatients
        self.nr = nr
        self.nd = nd
        self.surgeons = surgeons
        self.ors = ors
        self.start_times = start_times
        self.end_times = end_times

        self.capacity = 0
        for d in range(nd):
            for r in range(nr):
                self.capacity += self.ors[d][r].end - self.ors[d][r].start

        self.ncallbacks = 0
        self.blocks = []

        for d in range(nd):
            for r in range(nr):
                for b in self.ors[d][r].blocks:
                    self.blocks.append(b.duration)
        self.blocks = np.array(self.blocks)

        self.all_blocks = []
        self.block_durations = []
        for i in range(len(self.end_times)):
            self.all_blocks.append([])
            self.block_durations.append(self.end_times[i] - self.start_times[0])
        for b in range(self.blocks.shape[0]):
            ind = self.block_durations.index(self.blocks[b])
            self.all_blocks[ind].append(b)

        self.conflicts = []
        i = 0
        for d in range(self.nd):
            c_line = np.zeros(self.blocks.shape[0])
            for r in range(self.nr):
                c_line[i:i + len(self.ors[d][r].blocks)] = 1
                i = i + len(self.ors[d][r].blocks)
            self.conflicts.append(c_line)
        self.conflicts.append(np.ones(self.blocks.shape[0]))
        self.conflicts = np.array(self.conflicts)
        self.demands = np.ones(self.conflicts.shape[0])
        self.demands[-1] = self.nd  # dem_max

        pats = []
        all_pats = []
        for s in range(ns):
            pats_s = []
            for p in surgeons[s].patients:
                pats_s.append([p.duration, p.priority, p.master_priority])
                all_pats.append((p.duration, p.master_priority, s, len(pats_s)-1))
            pats_s = np.array(pats_s)
            pats.append(pats_s)
        self.patients = pats
        self.all_patients = sorted(all_pats, key=lambda tup: tup[1]^tup[0])

        i = 0
        cdrt = []
        for d in range(nd):
            for r in range(nr):
                for c in self.ors[d][r].conflicts:
                    if len(c) > 0:
                        c_line = np.zeros(self.blocks.shape[0])
                        c_line[i + np.array(c)] = 1
                        cdrt.append(c_line)
                i += len(ors[d][r].blocks)
        self.cdrt = np.array(cdrt)

        ## ----- initial patterns -----------
        w = []
        delta = []
        a = []
        fs = []
        itnum = []

        for s in range(ns):
            if len(self.patients[s])==0:
                w.append([0])
            else:
                ws = np.matmul(self.patients[s][:, 0], self.patients[s][:, 1])
                w.append([ws])
            delta.append([0])
            a.append([tuple([0]*self.blocks.shape[0])])
            fs.append([0])
            # itnum.append([0])
            # w.append([])
            # delta.append([])
            # a.append([])
            # fs.append([])

        self.w = w
        self.delta = delta
        self.a = a
        self.fs = fs
        # self.itnum = itnum

        ## --------------

        self.sub_time = 0
        self.mp_time = 0
        self.cb_time = 0
        self.time = 0

        self.nc = 0
        self.niterations = 0
        self.constrs = []
        for s in range(ns):
            self.constrs.append([])
        self.nLC = 0

    def col_gen(self, branch_conditions):
        ncallbacks = 0
        nc = 0
        niterations = 0
        nLC = 0
        mp_time = 0
        sub_time = 0
        cb_time = 0

        w = []
        delta = []
        a = []
        for s in range(self.ns):
            w_s = []
            delta_s = []
            a_s = []
            for k in range(len(self.w[s])):
                brc_ok = True
                for brc in branch_conditions[s]:
                    if self.a[s][k][brc[0]] != brc[1]:
                        brc_ok = False
                        break
                if brc_ok:
                    w_s.append(self.w[s][k])
                    delta_s.append(self.delta[s][k])
                    a_s.append(self.a[s][k])
            if True: #len(w_s)==0:
                # print("no patterns for surgeon ", s)
                br_o = np.zeros(self.blocks.shape[0])
                for brc in branch_conditions[s]:
                    br_o[brc[0]] = brc[1]
                br_o = tuple(br_o)
                if br_o in a_s:
                    w.append(w_s)
                    delta.append(delta_s)
                    a.append(a_s)
                    continue
                br_m = grb.Model()
                br_x = br_m.addVars(self.patients[s].shape[0], self.blocks.shape[0], vtype=GRB.BINARY)

                br_m.addConstrs(
                    grb.quicksum(br_x[i, b] for b in range(self.blocks.shape[0])) <= 1 for i in range(self.patients[s].shape[0]))
                br_m.addConstrs(grb.quicksum(br_x[p, j] * self.patients[s][p, 0] for p in range(self.patients[s].shape[0])) <= br_o[j]*self.blocks[j] for j in range(self.blocks.shape[0]))

                br_w = br_m.addVar(vtype=GRB.CONTINUOUS)
                br_m.addConstr(br_w == np.matmul(self.patients[s][:, 0], self.patients[s][:, 2]) - grb.quicksum(grb.quicksum((self.patients[s][p, 0] * self.patients[s][p, 2]) * br_x[p, j] for p in range(self.patients[s].shape[0])) for j in range(self.blocks.shape[0])))
                br_delta = br_m.addVar(vtype=GRB.CONTINUOUS)
                br_m.addConstr(br_delta == grb.quicksum(grb.quicksum(self.patients[s][p, 0] * br_x[p, j] for p in range(self.patients[s].shape[0])) for j in range(self.blocks.shape[0])))

                br_m.setObjective(grb.quicksum(grb.quicksum(self.patients[s][p, 1] * br_x[p, j] for p in range(self.patients[s].shape[0])) for j in range(self.blocks.shape[0]))
                               + self.smallM * self.alpha * br_delta - self.smallM * self.beta * br_w, GRB.MAXIMIZE)
                br_m.setParam('OutputFlag', False)
                br_m.optimize()

                w_s.append(br_w.x)
                a_s.append(br_o)
                delta_s.append(br_delta.x)

            w.append(w_s)
            delta.append(delta_s)
            a.append(a_s)


        sh = 1
        t = time.time()
        mp = primal_problem(w, delta, a, self.alpha, self.beta, GRB.CONTINUOUS, self.cdrt, self.timeout)

        if (mp.getAttr(GRB.Attr.Status) != GRB.OPTIMAL):
            return False, None, None, None, None

        mp_time += time.time() - t

        while sh > 0 and self.time + time.time()-t < self.timeout:
            niterations += 1
            sh = 0
            print(mp.objVal)
            # Pi = np.array(mp.getAttr('Pi', mp.getConstrs()))
            lambdas = len(self.cdrt)*[[0]]
            mus = self.ns*[[0]]
            for i in range(len(self.cdrt)):
                lambdas[i] = mp.getConstrByName('lambda'+str(i)).Pi
            for s in range(self.ns):
                mus[s] = mp.getConstrByName('mu'+str(s)).Pi
            # lambdas = Pi[:len(cdrt)]
            # mus = Pi[len(cdrt):len(cdrt) + ns]
            print(lambdas)
            print(mus)

            for s in range(self.ns):
                if len(self.patients[s])==0:
                    continue
                st = time.time()
                if self.recordLC:
                    shadows, os, ws, deltas, self.constrs[s], ncallbacks, optim, cbt = surgeon_subproblem(self.patients[s],self. blocks, lambdas,
                                                                                         self.conflicts, self.demands, self.alpha, self.beta,
                                                                                         self.cdrt, self.lc_version, self.all_blocks,
                                                                                         self.block_durations, self.nd, self.timeout,
                                                                                         mus[s], self.constrs[s], ncallbacks, branch_conditions[s])
                else:
                    shadows, os, ws, deltas, self.constrs[s], ncallbacks, optim, cbt = surgeon_subproblem(self.patients[s], self.blocks, lambdas,
                                                                                         self.conflicts, self.demands, self.alpha, self.beta,
                                                                                         self.cdrt, self.lc_version, self.all_blocks,
                                                                                         self.block_durations, self.nd, self.timeout,
                                                                                         mus[s], [], ncallbacks, branch_conditions[s])
                    nLC += len(self.constrs[s])
                sub_time += time.time()-st
                cb_time += cbt

                if len(os) != len(set([tuple(os[k]) for k in range(len(os))])):
                    print('adding duplicit columns: ', len(os), len(set([tuple(os[k]) for k in range(len(os))])))
                    for i in range(len(ws)):
                        print(ws[i], deltas[i], os[i])

                for i in range(len(ws)):
                    if shadows[i] > self.tolerance:
                        sh = 1
                        nc += 1
                        a[s].append(os[i])
                        w[s].append(ws[i])
                        delta[s].append(deltas[i])
                        print(shadows[i], nc)
            st = time.time()
            mp = primal_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, self.cdrt, self.timeout)
            mp_time += time.time() - st
            # st = time.time()
            # mp = primal_problem(w, delta, a, 1, 1, GRB.CONTINUOUS, cdrt, timeout)
            # mp_time += time.time() - st

        relaxed_first = self.capacity - mp._first.x
        relaxed_second = mp._second.x

        frac_s = 0
        frac_b = 0
        frac_x = 1
        for s in range(self.ns):
            xs = np.zeros(self.blocks.shape[0])
            for k in range(len(w[s])):
                for b in range(self.blocks.shape[0]):
                    xs[b] += mp._thetas[s][k].x*a[s][k][b]
            xs = abs(abs(xs) - 0.5)
            ind = np.argmin(xs)
            if xs[ind] < frac_x:
                frac_s = s
                frac_b = ind
                frac_x = xs[ind]

        mp = primal_problem(w, delta, a, 1, 1, GRB.INTEGER, self.cdrt, self.timeout)
        if (mp.getAttr(GRB.Attr.Status) != GRB.OPTIMAL):
            print("infeasible")
            return False, None, None, None, None

        for s in range(self.ns):
            self.a[s] += a[s]
            self.delta[s] += delta[s]
            self.w[s] += w[s]

        self.cb_time += cb_time
        self.mp_time += mp_time
        self.sub_time += sub_time
        self.time += time.time() - t
        self.nc += nc
        self.ncallbacks += ncallbacks
        self.nLC += nLC
        self.niterations += niterations
        return True, relaxed_first+relaxed_second, self.capacity-mp._first.x+mp._second.x, frac_s, frac_b

    def solve_instance(self):
        self.infeasibles = 0
        self.lb_all = []
        branch_conditions = []
        init_history = []
        for s in range(self.ns):
            branch_conditions.append([])
            init_history.append([])
        feasible, lb, ub, frac_s, frac_b  = self.col_gen(branch_conditions)

        self.current_node = Node(self.ns, lb, ub, frac_s, frac_b, init_history)
        self.best_node = 0
        self.lb = lb
        self.ub = np.round(ub)
        self.init_gap = ub - lb

        self.nodes.append(self.current_node)

        self.branch(self.current_node)
        return self.lb, self.ub, self.optim, len(self.nodes), self.time, self.nodes[self.best_node].history

    def branch(self, node):
        if self.optim or self.time>self.timeout:
            return

        level = int(np.log2(self.numnodes+1))
        if len(self.lb_all) <= level:
            self.lb_all.append([])

        self.numnodes += 1
        history_left = copy.deepcopy(node.history)
        history_left[node.s] += [(node.b, True)]
        feasible_left, lb_left, ub_left, frac_s_left, frac_b_left  = self.col_gen(history_left)
        history_right = copy.deepcopy(node.history)
        history_right[node.s] += [(node.b, False)]
        feasible_right, lb_right, ub_right, frac_s_right, frac_b_right = self.col_gen(history_right)
        if not feasible_left:
            # self.lb = lb_right
            print('infeasible: ', history_left)
            self.infeasibles += 1
            self.lb_all[level].append(np.inf)
        elif not feasible_right:
            # self.lb = lb_left
            print('infeasible: ', history_right)
            self.infeasibles += 1
            self.lb_all[level].append(np.inf)
        else:
            # self.lb = min(lb_left, lb_right)
            self.lb_all[level] += [lb_left, lb_right]

        if(len(self.lb_all[level]) == 2**level):
            self.lb = min(self.lb_all[level])

        mt = time.time()
        mp = primal_problem(self.w, self.delta, self.a, 1, 1, GRB.INTEGER, self.cdrt, self.timeout)
        self.mp_time += time.time() - mt
        self.ub = np.round(self.capacity - mp._first.x + mp._second.x)
        if abs(np.ceil(self.lb)-self.ub) < self.tolerance:
            self.optim = True
            return

        # branch left
        if feasible_left:
            lb, ub, frac_s, frac_b = lb_left, ub_left, frac_s_left, frac_b_left
            ub = np.round(ub)
            print('left lb: ', lb, 'current ub: ', self.ub)
            if np.ceil(lb) <= self.ub:
                self.current_node = Node(self.ns, lb, ub, frac_s, frac_b, history_left)
                self.nodes.append(self.current_node)
                if ub < self.ub:
                    self.ub = ub
                    self.best_node = len(self.nodes)-1
                    if abs(np.ceil(self.lb) - ub) < self.tolerance:
                        self.optim = True
                        return
                if np.ceil(lb) < self.ub:
                    print("branching left: ", len(self.nodes), frac_s, frac_b)
                    self.branch(self.current_node)

        # branch right
        if feasible_right:
            lb, ub, frac_s, frac_b = lb_right, ub_right, frac_s_right, frac_b_right
            ub = np.round(ub)
            print('right lb: ', lb, 'current ub: ', self.ub)
            if np.ceil(lb) <= self.ub:
                self.current_node = Node(self.ns, lb, ub, frac_s, frac_b, history_right)
                self.nodes.append(self.current_node)
                if ub < self.ub:
                    self.ub = ub
                    self.best_node = len(self.nodes)-1
                    if abs(np.ceil(self.lb) - ub) < self.tolerance:
                        self.optim = True
                        return
                if np.ceil(lb) < self.ub:
                    print("branching right: ", len(self.nodes), frac_s, frac_b)
                    self.branch(self.current_node)


    def heuristic_pattern(self, branching_conditions):
        surg_blocks = []
        for s in range(self.ns):
            surg_blocks.append([])
        blocks_assign = np.array(self.blocks.shape[0]*[None])

        for brc in branching_conditions:
            blocks_assign[brc[0]] = brc[1]

        for p in self.all_patients:     # p = (master_priority, duration, surgeon, patient)
            best_id = 0
            best_duration = 0
            for i, b in enumerate(surg_blocks[p[2]]): # b = (block_id, duration, vacancy)
                if b[2] >= p[1] and b[1]>best_duration:
                    best_id = i
                    best_duration = b[1]
            if best_duration>0:
                surg_blocks[s][best_id][2] -= p[2]
                continue
            block_candidates = []
            for i,d in enumerate(self.all_blocks):
                if blocks_assign[i]==None:
                    candidate = True
                    for i, c in enumerate(self.cdrt):
                        if sum((blocks_assign!=None)*c) > 1:
                            candidate = False
                            break
                    for i, c in enumerate(self.conflicts):
                        if sum((blocks_assign==p[2])*c) > self.demands[i]:
                            candidate = False
                            break
                    if candidate:
                        block_candidates.append((i, d))
            if len(block_candidates)>0:
                bid = np.argmax(block_candidates, axis=0)
                bid = bid[1]
                blocks_assign[bid] = p[2]
                surg_blocks[s].append([bid, self.blocks[bid], self.blocks[bid]-p[1]])

        for s in range(self.ns):
            if len(self.patients[s])==0:
                continue
            o = tuple((blocks_assign==s)-0)
            m = grb.Model()
            x = m.addVars(self.patients[s].shape[0], self.blocks.shape[0], vtype=GRB.BINARY)
            m.addConstrs(grb.quicksum(x[i, b] for b in range(self.blocks.shape[0])) <= 1 for i in range(self.patients[s].shape[0]))
            m.addConstrs(grb.quicksum(x[p, j] * self.patients[s][p, 0] for p in range(self.patients[s].shape[0])) <= o[j] * self.blocks[j] for j in range(self.blocks.shape[0]))
            w = m.addVar(vtype=GRB.CONTINUOUS)
            m.addConstr(w == np.matmul(self.patients[s][:, 0], self.patients[s][:, 2]) - grb.quicksum(grb.quicksum((self.patients[s][p, 0] * self.patients[s][p, 2]) * x[p, j] for p in range(self.patients[s].shape[0]))for j in range(self.blocks.shape[0])))
            delta = m.addVar(vtype=GRB.CONTINUOUS)
            m.addConstr(delta == grb.quicksum(grb.quicksum(self.patients[s][p, 0] * x[p, j] for p in range(self.patients[s].shape[0])) for j in range(self.blocks.shape[0])))
            m.setObjective(grb.quicksum(grb.quicksum(self.patients[s][p, 1] * x[p, j] for p in range(self.patients[s].shape[0])) for j in range(self.blocks.shape[0])) + self.smallM * self.alpha * delta - self.smallM * self.beta * w, GRB.MAXIMIZE)
            m.setParam('OutputFlag', False)
            m.optimize()

            self.w[s].append(w.x)
            self.a[s].append(o)
            self.delta[s].append(delta.x)
            # self.fs[s].append(m.objVal())
            print(w.x, delta.x, o)

def primal_problem(w, delta, a, alpha, beta, vtype, cdrt, timeout):
    ns = len(w)

    m = grb.Model()

    m._thetas = []
    for s in range(ns):
        ths = m.addVars(len(w[s]), lb=0,  vtype=vtype)
        m._thetas.append(ths)

    for i, c in enumerate(cdrt):
        m.addConstr(grb.quicksum(grb.quicksum(m._thetas[s][k] * np.matmul(c, a[s][k]) for k in range(len(w[s]))) for s in range(ns)) <= 1, name='lambda'+str(i))

    for s in range(ns):
        m.addConstr(grb.quicksum(m._thetas[s][k] for k in range(len(w[s]))) >= 1, name='mu'+str(s))

    m._first = m.addVar(vtype=vtype)
    m._second = m.addVar(vtype=vtype)
    m.addConstr(m._first <= grb.quicksum(grb.quicksum(m._thetas[s][k] * alpha*delta[s][k] for k in range(len(w[s]))) for s in range(ns)))
    m.addConstr(m._second >= grb.quicksum(grb.quicksum(m._thetas[s][k] * beta*w[s][k]  for k in range(len(w[s]))) for s in range(ns)))

    # m.setObjective(m._second - m._first + 0.001*grb.quicksum(grb.quicksum(m._thetas[s][k]* grb.quicksum(a[s][k][b]* b for b in range(nblocks)) for k in range(len(w[s]))) for s in range(ns)), grb.GRB.MINIMIZE)
    m.setObjective(m._second - m._first, grb.GRB.MINIMIZE)
    m.setParam('OutputFlag', False)
    m.Params.TimeLimit = timeout
    m.optimize()
    return m


def surgeon_subproblem(patients, blocks, lambdas, conflicts, demands, alpha, beta, cdrt, lc_version, all_blocks, block_durations, nd, timeout, mu, constrs, ncb, branch_conditions=[] ):
    # patients = [(duration, priority inner, priority outer)]
    # blocks = array of block lengths
    # conflicts =  matrix, set of blocks in each line marked by 1  - e.g. unavailability, max blocks per day
    # demands = max number of blocks assigned from each surgeon conflict
    # cdrt = matrix, set of conflicting blocks in each line marked by 1
    # all_blocks = block ids sorted by block lengths (for z variables)
    # block_durations = array of block durations
    # constrs = constraints on minimum surgeon objective: [number of assigned blocks for each length, min objective]


    bigM = 1000
    smallM = 0.001
    m = grb.Model()

    o = m.addVars(blocks.shape[0], vtype=GRB.BINARY) # surgeon blocks assignment
    x = m.addVars(patients.shape[0], blocks.shape[0], vtype=GRB.BINARY)  # patient block assignment

    m.addConstrs(grb.quicksum(conflicts[c,b] * o[b] for b in range(blocks.shape[0])) <= demands[c] for c in range(conflicts.shape[0]))
    m.addConstrs(grb.quicksum(x[i,b] for b in range(blocks.shape[0])) <= 1 for i in range(patients.shape[0]))
    m.addConstrs(grb.quicksum(x[p,j] * patients[p,0] for p in range(patients.shape[0])) <= o[j]*blocks[j] for j in range(blocks.shape[0]))

    # manage variables 'z'
    max_z = len(block_durations) * nd * block_durations[-1]  # maximum number of assigned blocks*max capacity to 1 surgeon
    # z = m.addVars(len(block_durations), max_z, vtype=grb.GRB.BINARY)
    # for l in range(len(block_durations)):
    #     m.addConstrs(z[l, k] >= z[l, k + 1] for k in range(max_z-1))#len(block_durations) * block_durations[-1] * nd - 1))

    # for bd in range(len(block_durations)):
    #     m.addConstr(z[bd, 0] == 1)
    #     if lc_version == 0:  ## orig
    #         m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(o[ab_id] for ab_id in all_blocks[bd]))
    #     elif lc_version == 1:  ## 1. redefinition
    #         m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) for j in
    #             range(bd, len(block_durations))))
    #     else:  ## 2. redefinition
    #         m.addConstr(grb.quicksum(z[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) * block_durations[j] for
    #             j in range(bd, len(block_durations))))

    q = m.addVars(len(block_durations), max_z, vtype=grb.GRB.BINARY)
    m.addConstrs(grb.quicksum(q[l,k] for k in range(max_z))==1 for l in range(len(block_durations)))

    for bd in range(len(block_durations)):
        if lc_version == 0:  ## orig
            m.addConstr(grb.quicksum(k*q[bd, k] for k in range(1, max_z)) == grb.quicksum(o[ab_id] for ab_id in all_blocks[bd]))
        elif lc_version == 1:  ## 1. redefinition
            m.addConstr(grb.quicksum(k*q[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) for j in
                range(bd, len(block_durations))))
        else:  ## 2. redefinition
            m.addConstr(grb.quicksum(k*q[bd, k] for k in range(1, max_z)) == grb.quicksum(grb.quicksum(o[ab_id] for ab_id in all_blocks[j]) * block_durations[j] for
                j in range(bd, len(block_durations))))

    m._w = m.addVar(vtype=GRB.CONTINUOUS)
    m.addConstr(m._w == np.matmul(patients[:, 0], patients[:, 2]) - grb.quicksum(
        grb.quicksum((patients[p, 0] * patients[p, 2]) * x[p, j] for p in range(patients.shape[0])) for j in
        range(blocks.shape[0])))
    m._delta = m.addVar(vtype=GRB.CONTINUOUS)
    m.addConstr(m._delta == grb.quicksum(
        grb.quicksum(patients[p, 0] * x[p, j] for p in range(patients.shape[0])) for j in range(blocks.shape[0])))

    for c in constrs:
        m.addConstr(grb.quicksum(grb.quicksum(x[p, b] for b in range(blocks.shape[0])) * patients[p, 1] for p in range(patients.shape[0]))
                    + smallM * alpha * m._delta - smallM * beta * m._w
                    + bigM * (len(block_durations) - grb.quicksum(grb.quicksum(q[bd, k] for k in range(c[bd], max_z) ) for bd in range(len(block_durations)))) >= c[-1])

    for brc in branch_conditions:
        # brc = (blockID, True/False)
        m.addConstr(o[brc[0]] == brc[1])

    m.setObjective( grb.quicksum(lambdas[i]*grb.quicksum(cdrt[i,b] * o[b] for b in range(blocks.shape[0])) for i in range(cdrt.shape[0])) - beta*m._w  + alpha*m._delta, GRB.MAXIMIZE)
                    # - smallM*grb.quicksum(b*o[b] for b in range(blocks.shape[0]))

    m.Params.lazyConstraints = 1
    m._blocks = blocks
    m._o = o
    m._x = x
    # m._z = z

    m._q = q

    m._block_durations = block_durations
    m._max_z = max_z
    m._patients = patients
    m.setParam('OutputFlag', False)
    m.Params.TimeLimit = timeout
    # m.setParam(GRB.Param.PoolSolutions, 5)
    # m.setParam(GRB.Param.PoolSearchMode, 2)
    m._scount = 0
    m._objs = []
    m._os = []
    m._ws = []
    m._deltas = []

    m._mu = mu
    m._lambdas = lambdas
    m._alpha = alpha
    m._beta = beta
    m._cdrt = cdrt
    m._constrs = constrs
    m._ncb = ncb
    m._optim = []
    m._cbtime = 0

    m.optimize(surgeon_callback)

    if m.getAttr(GRB.Attr.Status) != GRB.OPTIMAL:
        return m._objs, m._os, m._ws, m._deltas, m._constrs, m._ncb, m._optim, m._cbtime

    os = tuple([o[i].x for i in range(blocks.shape[0])])
    if m.objVal+mu>0 and os not in m._os:
        m._objs.append(m.objVal+mu)
        m._os.append(os)
        m._ws.append(m._w.x)
        m._deltas.append(m._delta.x)
        m._optim.append(0)

    return m._objs, m._os, m._ws, m._deltas, m._constrs, m._ncb, m._optim, m._cbtime


def surgeon_callback(model, where):
    bigM = 1000
    smallM = 0#0.001
    if where == grb.GRB.Callback.MIPSOL:
        st = time.time()
        model._ncb += 1
        m = grb.Model()
        x = m.addVars(model._patients.shape[0], model._blocks.shape[0], lb=0, vtype=GRB.BINARY)
        o = model.cbGetSolution(model._o)
        for b in range(model._blocks.shape[0]):
            o[b] = round(o[b])
        xp = model.cbGetSolution(model._x)
        sp_obj = grb.quicksum(model._lambdas[i] * grb.quicksum(model._cdrt[i,b] * o[b] for b in range(model._blocks.shape[0])) for i in range(model._cdrt.shape[0]))  + model._mu

        m.addConstrs(grb.quicksum(x[i, b] for b in range(model._blocks.shape[0])) <= 1 for i in range(model._patients.shape[0]))
        m.addConstrs(grb.quicksum(x[p, j] * model._patients[p, 0] for p in range(model._patients.shape[0])) <= o[j] * model._blocks[j] for j in range(model._blocks.shape[0]))

        w = m.addVar(vtype=GRB.CONTINUOUS)
        m.addConstr(w == np.matmul(model._patients[:,0],model._patients[:,2]) - grb.quicksum(grb.quicksum((model._patients[p,0]*model._patients[p,2]) * x[p,j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0])))
        delta = m.addVar(vtype=GRB.CONTINUOUS)
        m.addConstr(delta == grb.quicksum(grb.quicksum(model._patients[p,0] * x[p,j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0])))

        m.setObjective(grb.quicksum(grb.quicksum(model._patients[p, 1] * x[p, j] for p in range(model._patients.shape[0])) for j in range(model._blocks.shape[0]))
                       + smallM*model._alpha*delta - smallM*model._beta*w, GRB.MAXIMIZE)
        m.setParam('OutputFlag', False)
        m.optimize()
        if (m.getAttr(GRB.Attr.Status) != GRB.OPTIMAL):
            # print('Callback infeasible!!')
            return

        fs = 0
        for p in range(model._patients.shape[0]):
            for b in range(model._blocks.shape[0]):
                fs += round(xp[p,b]) * model._patients[p,1]

        mdelta = model.cbGetSolution(model._delta)
        mw = model.cbGetSolution(model._w)

        if m.objVal > fs+smallM*model._alpha*mdelta-smallM*model._beta*mw:
            z_count = []
            for j in range(len(model._block_durations)):
                for k in range(model._max_z):
                    if round(model.cbGetSolution(model._q[j,k])):
                        z_count.append(k)
                        break

            model.cbLazy(grb.quicksum(grb.quicksum(model._x[p,b] for b in range(model._blocks.shape[0])) * model._patients[p,1] for p in range(model._patients.shape[0]))
                         + smallM * model._alpha * model._delta - smallM * model._beta * model._w
                         + bigM * (len(model._block_durations) - grb.quicksum(grb.quicksum(model._q[bd, k] for k in range(z_count[bd], model._max_z) ) for bd in range(len(model._block_durations)))) >= m.objVal)
            model._constrs.append(z_count+[m.objVal])

            sp_obj -= model._beta*w.x
            sp_obj += model._alpha*delta.x
            if False and sp_obj > 0 and tuple([o[b] for b in range(model._blocks.shape[0])]) not in model._os:
                model._scount += 1
                model._objs.append(sp_obj)
                model._os.append(tuple([o[b] for b in range(model._blocks.shape[0])]))
                model._ws.append(w.x)
                model._deltas.append(delta.x)
                model._optim.append(m.objVal)
                # model.terminate()

        # elif (sp_obj - model._beta * mw + model._alpha * mdelta) > 0 and tuple([o[b] for b in range(model._blocks.shape[0])]) not in model._os:
        elif False and model.cbGet(GRB.Callback.MIPSOL_OBJBST) + model._mu > 0 and tuple(
                    [o[b] for b in range(model._blocks.shape[0])]) not in model._os:
            model._scount += 1
            model._objs.append(sp_obj - model._beta * mw + model._alpha * mdelta)
            model._os.append(tuple([o[b] for b in range(model._blocks.shape[0])]))
            model._ws.append(mw)
            model._deltas.append(mdelta)
            model._optim.append(m.objVal)
            # model.terminate()
        model._cbtime += time.time()-st
        # if fs > m.objVal:
        #     print('Error!', m.objVal, fs)
            # model.terminate()



def solve_instance(in_filename, lc_version, tolerance, timeout, alpha, beta, recordLC):
    inst = Instance(in_filename, lc_version, tolerance, timeout, alpha, beta, recordLC)
    inst.heuristic_pattern([])
    inst.solve_instance()
    return inst.time, inst.lb, inst.ub, inst.nc, inst.niterations, inst.sub_time, inst.mp_time, inst.cb_time, inst.ncallbacks, inst.nLC, inst.numnodes, inst.init_gap

if __name__=="__main__":
    # t, rfirst, rsecond, ifirst, isecond, nc, nit, sub_time, mp_time, cb_time, ncb, nLC, dup = solve_instance('../data/1_RealLifeSurgeryTypesDatabase/random_priority/73_ordays_5_load_1,15surgeons_5.txt', 0, 0.001, 200, 1, 1, 1)
    # print(t, rfirst, rsecond, ifirst, isecond, nc, nit, sub_time, mp_time, cb_time, ncb, nLC, dup)

    t = time.time()
    # inst = Instance('../data/1_RealLifeSurgeryTypesDatabase/random_priority/51_ordays_5_load_1,05surgeons_5.txt', 0, 0.001, 1800, 1, 1, 1)
    # inst.heuristic_pattern([])
    # inst.solve_instance()
    # print(time.time()-t)
    # print(inst.optim)
    # print(inst.time, inst.lb, inst.ub, inst.nc, inst.niterations, inst.sub_time, inst.mp_time, inst.cb_time, inst.ncallbacks, inst.nLC, inst.numnodes, inst.infeasibles)
    # mp = primal_problem(inst.w, inst.delta, inst.a, 1, 1, GRB.INTEGER, inst.cdrt, inst.timeout)
    # print(inst.capacity - mp._first.x + mp._second.x)
    # print(inst.blocks)
    print(solve_instance('../data/1_RealLifeSurgeryTypesDatabase/random_priority/53_ordays_5_load_1,05surgeons_5.txt', 0, 0.001, 1800, 1, 1, 1))