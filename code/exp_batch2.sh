#!/bin/bash
#SBATCH --job-name=nr4-const
#SBATCH --output=/home/nguyemi5/or-scheduling/out/nr4-const.out
#SBATCH --cpus-per-task=10
#SBATCH --mem=16G
#SBATCH --time=1-12:10:00
#SBATCH --mail-user=nguyemi5@fel.cvut.cz
#SBATCH --mail-type=ALL

module load Python/3.7.2-GCCcore-8.2.0
module load Gurobi/9.0.0

pip3 install numpy --user

cd /home/nguyemi5/or-scheduling/code/
python3 modelilp_week1.py 20 0 4 constant_priority 3600
python3 modelilp_week1.py 20 1 4 constant_priority 3600