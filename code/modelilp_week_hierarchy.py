from parse_instance import parse_in
import gurobipy as gb
import copy
import time

## setup options
filename = "in_choir5s5d1r_1__.txt"   # 0 = 100%, 1 = 80%, 2 = 70%, 3 = 50% capacity restriction
lc_version = 2      # 0 = original 1 = 1st redefinition, 2 = 2nd redefinition
added_obj = 0

## monitoring variables
ncallbacks = 0
n_lc = 0
cb_time = 0

def callback_lazy(model, where):
    global ncallbacks, n_lc, cb_time # count real callbacks and added LC

    if where == gb.GRB.Callback.MIPSOL: # check if current solution is optimal
        ss = time.time()
        ncallbacks += 1
        # generate new lazy constraint for each surgeon
        for i in range(ns):
            ss = time.time()
            # extract blocks assigned to surgeon i
            assigned_blocks = []
            for d in range(nd):
                for r in range(nr):
                    for b in range(len(ors[d][r].blocks)):
                        if round(model.cbGetSolution(y[d][r][i,b])) == 1:
                            assigned_blocks.append(ors[d][r].blocks[b].duration)

            # initiate a model to calculate an optimal patient selection
            fmodel = gb.Model()
            xf = fmodel.addVars(len(surgeons[i].patients), len(assigned_blocks), vtype=gb.GRB.BINARY)
            # duration of assigned block cannot be exceeded
            fmodel.addConstrs(gb.quicksum(xf[j,b]*surgeons[i].patients[j].duration for j in range(len(surgeons[i].patients))) <= assigned_blocks[b] for b in range(len(assigned_blocks)))
            # each patient can be operated only once
            fmodel.addConstrs(gb.quicksum(xf[j,b] for b in range(len(assigned_blocks))) <= 1 for j in range(len(surgeons[i].patients)))
            # the objective is to operate the highest priority patients
            fmodel.setObjective(gb.quicksum(gb.quicksum(xf[j,b] for b in range(len(assigned_blocks)))*surgeons[i].patients[j].priority for j in range(len(surgeons[i].patients))), gb.GRB.MAXIMIZE)
            fmodel.optimize()
            # calculate value of assignment in the current solution
            fs = 0
            for pat in surgeons[i].patients:
                pat_scheduled = 0
                for d in range(nd):
                    for r in range(nr):
                        for b in range(len(ors[d][r].blocks)):
                            pat_scheduled += round(model.cbGetSolution(x[d][r][pat.id, b]))
                fs += pat_scheduled*pat.priority

            print('surgeon: ', i, ', fs: ', fs, ', opt: ', round(fmodel.objVal))
            # if the current solution is not optimal, add a lazy constraint
            if round(fmodel.objVal) > fs:
                n_lc += 1
                # count z in current assignment
                z_count = []
                for j in range(len(block_durations)):
                    z_temp = 0
                    for k in range(max_z):
                        z_temp += round(model.cbGetSolution(z[i,j,k]))
                    z_count.append(z_temp-1)

                fs = round(fmodel.objVal)
                model.cbLazy(gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][pat.id,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd))*pat.priority for pat in surgeons[i].patients)
                             + bigM*(len(block_durations) - gb.quicksum(z[i,bd,z_count[bd]] for bd in range(len(block_durations)))) >= fs )
                # break
        cb_time += time.time() - ss

ns, np, nr, nd, surgeons, patients, ors, start_times, end_times, unavailability = parse_in(filename)
unavailability = []

# sort blocks according to length (to manage variables 'z')
all_blocks = []
block_durations = []
for i in range(len(end_times)):
    all_blocks.append([])
    block_durations.append(end_times[i]-start_times[0])
for d in range(nd):
    for r in range(nr):
        for b in ors[d][r].blocks:
            ind = block_durations.index(b.duration)
            all_blocks[ind].append((d,r,b.id))

model = gb.Model()
model.Params.lazyConstraints = 1
y = []
x = []
bigM = 1000

for i in range(nd):
    yd = []
    xd = []
    for r in range(nr):
        yr = model.addVars(ns, len(ors[i][r].blocks), vtype=gb.GRB.BINARY)      # surgeon - block assignment
        xr = model.addVars(np, len(ors[i][r].blocks), vtype=gb.GRB.BINARY)      # patient - block assignment
        yd.append(yr)
        xd.append(xr)
    y.append(yd)
    x.append(xd)

# manage variables 'z'
max_z = len(block_durations) * nd * block_durations[-1]  # maximum number of assigned blocks*max capacity to 1 surgeon
z = model.addVars(ns, len(block_durations), max_z, vtype=gb.GRB.BINARY)
for i in range(ns):
    for l in range(len(block_durations)):
        model.addConstrs(z[i,l,k] >= z[i,l,k+1] for k in range(len(block_durations)*block_durations[-1]*nd-1))

for i in range(ns):
    for bd in range(len(block_durations)):
        model.addConstr(z[i,bd,0] == 1)
        if lc_version == 0:   ## orig
            model.addConstr(gb.quicksum(z[i, bd, k] for k in range(1, max_z)) == gb.quicksum(y[ab_id[0]][ab_id[1]][i, ab_id[2]] for ab_id in all_blocks[bd]))
        elif lc_version == 1:   ## 1. redefinition
            model.addConstr(gb.quicksum(z[i, bd, k] for k in range(1, max_z)) == gb.quicksum(gb.quicksum(y[ab_id[0]][ab_id[1]][i, ab_id[2]] for ab_id in all_blocks[j]) for j in range(bd, len(block_durations))))
        else:   ## 2. redefinition
            model.addConstr(gb.quicksum(z[i, bd, k] for k in range(1, max_z)) == gb.quicksum(gb.quicksum(y[ab_id[0]][ab_id[1]][i, ab_id[2]] for ab_id in all_blocks[j])*block_durations[j] for j in range(bd, len(block_durations))))

# every surgeon can be assigned 1 block a day, every patient can be operated only once
for d in range(nd):
    model.addConstrs(gb.quicksum(gb.quicksum(y[d][r][i, b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) <= 1 for i in range(ns))
model.addConstrs(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j, b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)) <= 1 for j in range(np))

# surgeon's unavailability
if len(unavailability) > 0:
    for i in range(ns):
        model.addConstrs(gb.quicksum(gb.quicksum(y[d][r][i,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) == 0 for d in unavailability[i] )

for d in range(nd):
    for r in range(nr):
        # manage conflicting blocks (same time, same OR)
        for k in range(len(ors[d][r].conflicts)):
            if len(ors[d][r].conflicts[k])==0:
                continue
            model.addConstr(gb.quicksum(gb.quicksum(y[d][r][i,b] for i in range(ns)) for b in ors[d][r].conflicts[k])==1)
        for b in range(len(ors[d][r].blocks)):
            # capacity of the block b assigned to surgeon s cannot be exceeded
            model.addConstrs(gb.quicksum(x[d][r][pat.id, b]*pat.duration for pat in surgeons[i].patients) <= ors[d][r].blocks[b].duration*y[d][r][i, b] for i in range(ns))
            # patient can be operated only in the blocks assigned to their surgeon
            # model.addConstrs(gb.quicksum(x[d][r][pat.id, b] for pat in surgeons[i].patients) <= bigM * y[d][r][i, b] for i in range(ns))
            # block duration cannot be exceeded
            # model.addConstr(gb.quicksum(x[d][r][j, b] * patients[j].duration for j in range(np)) <= ors[d][r].blocks[b].duration)

capacity = 0
for d in range(nd):
    for r in range(nr):
        capacity += ors[d][r].end - ors[d][r].start
alpha = 1
beta = 1
model.setObjective(alpha*(capacity - gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j,b]*patients[j].duration for j in range(np)) for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd))) +
                   beta*(gb.quicksum((1-gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][j,b] for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)))*patients[j].duration for j in range(np))), gb.GRB.MINIMIZE)

if added_obj>0:
    for i in range(added_obj):
        model.setObjectiveN(gb.quicksum(gb.quicksum(gb.quicksum(gb.quicksum(x[d][r][pat.id,b]*patients[pat.id].priority for b in range(len(ors[d][r].blocks))) for r in range(nr)) for d in range(nd)) for pat in surgeons[i].patients), i+1, 1, -1)
    model.setParam(gb.GRB.Param.ObjNumber, 0)
    model.ObjNPriority = 2


s = time.time()
model.optimize(callback_lazy)

# model.optimize()
print('optimization time: ', time.time()-s)
print('objective: ', model.objVal)
for d in range(nd):
    print('DAY: ', d)
    for r in range(nr):
        print(' OR: ', r, ors[d][r].start, ors[d][r].end)
        for b in range(len(ors[d][r].blocks)):
            for i in range(ns):
                if y[d][r][i,b].x > 0.5:
                    print('     surgeon: ', i, 'start: ', ors[d][r].blocks[b].start, 'end: ', ors[d][r].blocks[b].end)
                    print('         patients: ', end='')
                    for pat in surgeons[i].patients:
                        if x[d][r][pat.id,b].x > 0.5:
                            print(pat.id, '(', pat.duration, ')', end='; ')
                    print('')

print('unscheduled: ')
for j in range(np):
    pat_scheduled = 0
    for d in range(nd):
        for r in range(nr):
            for b in range(len(ors[d][r].blocks)):
                pat_scheduled += round(x[d][r][j, b].x)
    if pat_scheduled == 0:
        print('{} {} {} {}'.format(patients[j].id, patients[j].surgeon, patients[j].priority, patients[j].duration))

print('# callbacks: ', ncallbacks)
print('# LC: ', n_lc)
print('cb time: ', cb_time)