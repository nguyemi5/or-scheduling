#!/bin/bash
#SBATCH --job-name=r1s5rand
#SBATCH --output=/home/nguyemi5/or-scheduling/out/r1s5rand.out
#SBATCH --cpus-per-task=10
#SBATCH --mem=8G
#SBATCH --time=0-20:00:00
#SBATCH --mail-user=nguyemi5@fel.cvut.cz
#SBATCH --mail-type=ALL

module load Python/3.7.2-GCCcore-8.2.0
module load Gurobi/9.0.0

pip3 install numpy --user

cd /home/nguyemi5/or-scheduling/code/
python3 modelilp_week1.py 5 0 1 random_priority 1800
python3 modelilp_week1.py 5 1 1 random_priority 1800
python3 modelilp_week1.py 5 2 1 random_priority 1800


